FROM node:10.10.0

WORKDIR /app

EXPOSE 4000

ARG VERSION=latest

# default prisma.yml config
COPY ./.defaultprisma.yml prisma.yml

# install prisma
RUN yarn global add prisma

COPY package.json yarn.lock entrypoint.sh wait-for-it.sh ./

RUN NODE_ENV=development yarn install

COPY . ./

RUN yarn build

ENTRYPOINT ["./entrypoint.sh"]

RUN echo $VERSION > version

CMD ["node", "./dist/index.js"]