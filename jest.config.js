module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    setupTestFrameworkScriptFile: "<rootDir>src/test/setupTests.js",
    globalSetup: "<rootDir>src/test/globalSetup.js",
    globalTeardown: "<rootDir>src/test/globalTeardown.js",
    testResultsProcessor: './node_modules/jest-junit-reporter'
};