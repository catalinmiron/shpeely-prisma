import * as jwt from 'jsonwebtoken'
import { Prisma, Tournament } from './generated/prisma'

export interface Context {
  db: Prisma
  request: any
  tournament?: Tournament
}

export interface Auth0UserId {
  auth0id: string
}

export function getUserId(ctx: Context): Auth0UserId {
  const Authorization = ctx.request.get('Authorization')
  if (Authorization) {
    const token = Authorization.replace('Bearer ', '')
    const { user_id } = jwt.verify(token, process.env.AUTH0_CLIENT_SECRET) as {
      user_id: string
    }

    try {
      return {
        auth0id: user_id.split('|')[1]
      }
    } catch (err) {
      throw new Error('Not a valid auth0 id token')
    }
  }

  throw new AuthError()
}

export class AuthError extends Error {
  constructor() {
    super('Not authorized')
  }
}
