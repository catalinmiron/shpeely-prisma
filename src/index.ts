import server from './server'
import * as logger from 'winston'

server.start({ debug: true }, () =>
  logger.info(`Server is running on http://localhost:4000`)
)
