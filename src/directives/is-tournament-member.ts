import { SchemaDirectiveVisitor } from 'graphql-tools'
import * as logger from 'winston'
import { getUserFromCtx } from './helpers'
import { GraphQlError } from '../errors'
import { Context } from '../utils'

export class IsTournamentMemberDirective extends SchemaDirectiveVisitor {
  tournamentSelection = '{ slug members { user { auth0id } role } }'

  public visitFieldDefinition(field) {
    const resolve = field.resolve

    field.resolve = async (parent, args, ctx: Context, info) => {
      const { auth0id } = getUserFromCtx(ctx)
      const { role } = this.args

      if (args.data && args.data.slug) {
        // tournament slug is in the args
        logger.debug('Inferred tournament from the arguments')

        const tournament = await ctx.db.query.tournament(
          {
            where: { slug: args.data.slug }
          },
          this.tournamentSelection
        )

        this.validateIsTournamentMember(auth0id, tournament, role)

        ctx.tournament = tournament
      } else if (
        info.fieldName &&
        info.variableValues &&
        info.variableValues.slug
      ) {
        // Field -> tournament slug is in the parent args
        logger.debug('Inferred tournament from the parent arguments')

        const tournament = await ctx.db.query.tournament(
          {
            where: { slug: info.variableValues.slug }
          },
          this.tournamentSelection
        )

        this.validateIsTournamentMember(auth0id, tournament, role)

        ctx.tournament = tournament

        return parent[info.fieldName]
      } else {
        // infer tournament from the query and return type
        logger.debug('Inferred tournament from the query and return type')

        // first letter lowercased is the prisma function name. Hacky solution but I can't find a better way.
        const { tournament } = await ctx.db.query[
          (info.returnType.name || info.returnType.ofType.name).replace(
            /^./,
            str => str.toLowerCase()
          )
        ](
          args,
          `
            {
                tournament ${this.tournamentSelection}
            }
            `
        )

        this.validateIsTournamentMember(auth0id, tournament, role)

        ctx.tournament = tournament
      }

      // call the resolver
      return await resolve.apply(field, [parent, args, ctx, info])
    }
  }

  private validateIsTournamentMember(auth0id, tournament, role?) {
    const member = tournament.members.find(
      ({ user: { auth0id: id } }) => auth0id === id
    )
    if (!member) {
      throw new Error(GraphQlError.NOT_TOURNAMENT_MEMBER)
    } else if (role && member.role !== role) {
      throw new Error(GraphQlError.NOT_ALLOWED_BASED_ON_ROLE)
    }
  }
}
