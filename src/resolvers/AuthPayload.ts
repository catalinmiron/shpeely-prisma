import { Context } from '../utils'

export const AuthPayload = {
  user: async ({ user: { auth0id } }, args, ctx: Context, info) => {
    return ctx.db.query.user({ where: { auth0id } }, info)
  }
}
