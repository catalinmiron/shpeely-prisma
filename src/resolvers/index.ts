import { Query } from './Query'
import { Subscription } from './Subscription'
import { auth } from './Mutation/auth'
import { AuthPayload } from './AuthPayload'
import { tournament } from './Mutation/tournament'
import { gameresult } from './Mutation/gameresult'
import { invitation } from './Mutation/invitation'
import { tournamentMember } from './Mutation/tournament-member'

export default {
  Query: {
    ...Query
  },
  Mutation: {
    ...auth,
    ...gameresult,
    ...tournament,
    ...invitation,
    ...tournamentMember
  },
  // Subscription,
  AuthPayload
}
