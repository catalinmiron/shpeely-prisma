import { validateAndParseIdToken } from '../../helpers/authHelpers'
import { Auth0, UserProfile } from '../../services/auth0'

async function createPrismaUser(ctx, userToken, profile: UserProfile) {
  const user = await ctx.db.mutation.createUser({
    data: {
      email: profile.email,
      username:
        profile.name ||
        profile.nickname ||
        `User ${Math.round(Math.random() * 10000000)}`,
      avatar: profile.picture,
      auth0id: userToken.sub
    }
  })
  return user
}

export interface UserProfile {
  avatar: string
  given_name?: string
  family_name?: string
  name?: string
  nickname: string
  picture: string
  gender: string
  updated_at: string // ISO-8601
  email: string
  email_verified: boolean
}

export const auth = {
  async authenticate(parent, { idToken }, ctx, info) {
    let userToken = null
    try {
      userToken = await validateAndParseIdToken(idToken)
    } catch (err) {
      throw new Error(err.message)
    }

    const profile: UserProfile = {
      avatar: userToken.picture,
      given_name: userToken.given_name,
      family_name: userToken.family_name,
      name: userToken.name,
      nickname: userToken.nickname,
      picture: userToken.picture,
      gender: userToken.gender,
      updated_at: userToken.updated_at, // ISO-8601
      email: userToken.email,
      email_verified: userToken.email_verified
    }

    const auth0id = userToken.sub

    let user = await ctx.db.query.user({ where: { auth0id } }, info)
    if (!user) {
      // add more profile data if the token allows it
      let profileDetails = {}
      try {
        profileDetails = await Auth0.loadUserProfile(idToken)
      } catch {
        // ignore
      }

      user = createPrismaUser(ctx, userToken, {
        ...profile,
        ...profileDetails
      })
    }

    return user
  }
}
