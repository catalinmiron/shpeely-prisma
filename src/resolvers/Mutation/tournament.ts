import { forwardTo } from 'prisma-binding'
import * as getSlug from 'speakingurl'
import { GraphQLResolveInfo } from 'graphql'
import { Context } from '../../utils'
import { Prisma, Tournament } from '../../generated/prisma'

async function getUniqueSlug(slug: string, db: Prisma): Promise<string> {
  // create a unique slug from the tournament name
  let isUnique = false
  let counter = 1
  let uniqueSlug: string

  while (!isUnique) {
    if (counter === 1) {
      uniqueSlug = getSlug(slug)
    } else {
      uniqueSlug = getSlug(`${slug} ${counter}`)
    }

    const tournament = await db.query.tournament({
      where: { slug: uniqueSlug }
    })
    isUnique = tournament === null

    counter += 1
  }

  return uniqueSlug
}

export const tournament = {
  createTournament: async (
    parent: any,
    args: any,
    ctx: Context,
    info: GraphQLResolveInfo
  ) => {
    const slug = await getUniqueSlug(args.data.name, ctx.db)

    return ctx.db.mutation.createTournament(
      {
        data: {
          ...args.data,
          slug,
          members: {
            create: {
              role: 'ADMIN',
              user: {
                connect: {
                  auth0id: ctx.request.user.auth0id
                }
              }
            }
          }
        }
      },
      info
    )
  }
}
