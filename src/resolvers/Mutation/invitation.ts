import * as crypto from 'crypto'
import { GraphQLResolveInfo } from 'graphql'
import { Context } from '../../utils'
import { forwardTo } from 'prisma-binding'
import { Mailer } from '../../services/mailer'
import * as logger from 'winston'
import { InvitationState } from '../../generated/prisma'
import { GraphQlError } from '../../errors'

const acceptInvitationLink = `${process.env.URL}${process.env.INVITATION_PATH}`

export const invitation = {
  rejectInvitation: async (
    parent: any,
    args: { where: { token: string } },
    ctx: Context,
    info: GraphQLResolveInfo
  ) => {
    return ctx.db.mutation.updateInvitation(
      {
        where: { token: args.where.token },
        data: { state: 'REJECTED' }
      },
      info
    )
  },
  acceptInvitation: async (
    parent: any,
    args: { where: { token: string } },
    ctx: Context,
    info: GraphQLResolveInfo
  ) => {
    // load invitation
    const {
      tournament: { slug, members },
      state
    } = await ctx.db.query.invitation(
      {
        where: { token: args.where.token }
      },
      `{
			state
			tournament {
				slug
				members {
					user {
						auth0id
					}
				}
			}
		}`
    )

    if (members.find(m => m.user.auth0id === ctx.request.user.auth0id)) {
      throw new Error(GraphQlError.ALREADY_MEMBER)
    }

    if (state !== 'PENDING') {
      throw new Error(GraphQlError.INVITATION_ALREADY_RESPONDED_TO)
    }

    await ctx.db.mutation.createTournamentMember({
      data: {
        user: {
          connect: {
            auth0id: ctx.request.user.auth0id
          }
        },
        tournament: {
          connect: {
            slug
          }
        }
      }
    })

    return ctx.db.mutation.updateInvitation(
      {
        where: { token: args.where.token },
        data: { state: 'ACCEPTED' }
      },
      info
    )
  },
  createInvitation: async (
    parent: any,
    args: { data: { email: string; slug: string } },
    ctx: Context,
    info: GraphQLResolveInfo
  ) => {
    // load tournament
    const { name: tournamentName, invitations } = await ctx.db.query.tournament(
      { where: { slug: args.data.slug } },
      `{
				name
				invitations {
					token
					email
				}
			}`
    )
    // check if user is already invited:
    const existingInvitation = invitations.find(
      x => x.email === args.data.email && x.state === 'PENDING'
    )

    if (existingInvitation) {
      logger.debug(
        `Invitation was already sent to the email address ${
          args.data.email
        }. ` + `Returning existing invitation...`
      )
      return ctx.db.query.invitation(
        { where: { token: existingInvitation.token } },
        info
      )
    }

    // create random token
    const token = crypto.randomBytes(16).toString('hex')

    Mailer.sendMail(
      args.data.email,
      `You have been invited to join the tournament ${tournamentName} on Shpeely!`,
      `${ctx.request.user.username} ` +
        `invited you to join the tournament ${tournamentName} on Shpeely. ` +
        `To accept the invitation please click this link: ` +
        `<a href="${acceptInvitationLink}/${token}">${acceptInvitationLink}/${token}</a>`
    )

    return ctx.db.mutation.createInvitation(
      {
        data: {
          token,
          email: args.data.email,
          tournament: {
            connect: {
              slug: args.data.slug
            }
          },
          inviter: {
            connect: {
              auth0id: ctx.request.user.auth0id
            }
          }
        }
      },
      info
    )
  }
}
