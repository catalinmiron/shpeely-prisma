jest.mock('../../services/mailer')

import server from '../../server'
import { GraphQlTestClient } from '../../test/graphQlTestClient'
import { GraphQlError } from '../../errors'
import {
  acceptInvitation,
  createInvitation,
  createRandomTournament,
  createTournament,
  expectGraphQlError
} from '../../test/testUtils'

describe('Invitation Mutations', () => {
  const testuser1 = {
    username: process.env.TEST_USER,
    password: process.env.TEST_PASSWORD
  }

  const testuser2 = {
    username: process.env.TEST_USER2,
    password: process.env.TEST_PASSWORD2
  }

  const port = 4002

  const sendMailMock = jest.fn()

  beforeEach(() => {
    const { Mailer } = require('../../services/mailer')
    Mailer.__setSendMailFn(sendMailMock)
  })

  afterEach(() => {
    sendMailMock.mockReset()
  })

  describe('Anonymous', () => {
    let client
    let tournament

    beforeAll(async () => {
      const app = await server.start({ port })

      client = new GraphQlTestClient(app)
      await client.authenticate(testuser1.username, testuser1.password)
      tournament = await createTournament(client, 'Some Tournament')
      client.unauthenticate()
    })

    afterAll(() => client.close())

    test('Should throw a login required error', async () => {
      const { slug } = tournament

      const query = `
				mutation ($email: String!, $slug: String!) {
					createInvitation(data: {email: $email, slug: $slug}) {
						state
					}
				}
			`

      const variables = {
        slug,
        email: 'info@shpeely.com'
      }

      await expectGraphQlError(
        async () => await client.request(query, variables),
        GraphQlError.LOGIN_REQUIRED
      )
    })
  })

  describe('Authenticated', () => {
    let client
    let tournament

    beforeAll(async () => {
      const app = await server.start({ port })
      client = new GraphQlTestClient(app)
    })

    beforeEach(async () => {
      await client.authenticate(testuser1.username, testuser1.password)
      tournament = await createRandomTournament(client)
    })

    afterAll(() => client.close())

    test('Should create an invitation and send an email', async () => {
      const invitation = await createInvitation(
        client,
        tournament.slug,
        'info@shpeely.com'
      )

      expect(sendMailMock).toHaveBeenCalled()
    })

    test('Should add the new user to the tournament after accepting the invitation', async () => {
      const invitation = await createInvitation(
        client,
        tournament.slug,
        'info@shpeely.com'
      )

      await client.authenticate(testuser2.username, testuser2.password)
      await acceptInvitation(client, invitation.token)

      const {
        tournament: { members }
      } = await client.request(`
				query {
					tournament(where: { slug: "${tournament.slug}" }) {
						members {
							user {
								username
							}
						}
					}
				}
			`)

      expect(members).toContainEqual({
        user: { username: testuser2.username }
      })
    })

    test('Should throw an error if an existing member tries to accept an invitation', async () => {
      const invitation = await createInvitation(
        client,
        tournament.slug,
        'info@shpeely.com'
      )

      await expectGraphQlError(
        async () => await acceptInvitation(client, invitation.token),
        GraphQlError.ALREADY_MEMBER
      )
    })

    test('Should throw an error if an invitation is responded to more than once', async () => {
      const invitation = await createInvitation(
        client,
        tournament.slug,
        'testuser@example.com'
      )

      await client.authenticate(testuser2.username, testuser2.password)
      await acceptInvitation(client, invitation.token)

      await expectGraphQlError(
        async () => await acceptInvitation(client, invitation.token),
        GraphQlError.INVITATION_ALREADY_RESPONDED_TO
      )
    })

    test('Only tournament members should be able to create invitations for a tournament', async () => {
      await client.authenticate(testuser2.username, testuser2.password)

      await expectGraphQlError(
        async () =>
          await createInvitation(
            client,
            tournament.slug,
            'testuser2@example.com'
          ),
        GraphQlError.NOT_TOURNAMENT_MEMBER
      )
    })

    test('Users that are not member of the tournament should not see the invitation token', async () => {
      const invitation = await createInvitation(
        client,
        tournament.slug,
        'testuser2@example.com'
      )

      await client.authenticate(testuser2.username, testuser2.password)

      const { slug } = tournament
      const doRequest = async () => {
        await client.request(
          `
				query($slug: String!) {
					tournament(where: { slug: $slug }) {
						invitations {
							token
						}
					}
				}
			`,
          { slug }
        )
      }

      await expectGraphQlError(doRequest, GraphQlError.NOT_TOURNAMENT_MEMBER)
    })
  })
})
