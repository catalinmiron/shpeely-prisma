jest.mock('../../services/mailer')

import server from '../../server'
import { GraphQlTestClient } from '../../test/graphQlTestClient'
import { GraphQlError } from '../../errors'
import {
  acceptInvitation,
  createGameresult,
  createInvitation,
  createRandomTournament,
  deleteGameResult,
  expectGraphQlError,
  loadGameResult,
  updateGameResult
} from '../../test/testUtils'

describe('Gameresult Mutations', () => {
  const testuser1 = {
    username: process.env.TEST_USER,
    password: process.env.TEST_PASSWORD
  }

  const testuser2 = {
    username: process.env.TEST_USER2,
    password: process.env.TEST_PASSWORD2
  }

  const port = 4002

  beforeEach(() => {
    const { Mailer } = require('../../services/mailer')
  })

  describe('Anonymous', () => {
    let client
    let tournament

    beforeAll(async () => {
      const app = await server.start({ port })

      client = new GraphQlTestClient(app)
      await client.authenticate(testuser1.username, testuser1.password)
      tournament = await createRandomTournament(client)
      client.unauthenticate()
    })

    afterAll(() => client.close())

    test('Should throw a login required error when not logged in', async () => {
      const { slug } = tournament

      const doRequest = async () => await createGameresult(client, slug)

      const variables = {
        slug,
        email: 'info@shpeely.com'
      }

      await expectGraphQlError(doRequest, GraphQlError.LOGIN_REQUIRED)
    })
  })

  describe('Authenticated', () => {
    let client
    let tournament

    beforeAll(async () => {
      const app = await server.start({ port })
      client = new GraphQlTestClient(app)
    })

    beforeEach(async () => {
      await client.authenticate(testuser1.username, testuser1.password)
      tournament = await createRandomTournament(client)
    })

    afterAll(() => client.close())

    test('Should create a new game result and the players', async () => {
      const result = await createGameresult(client, tournament.slug, 31260, [
        {
          playerName: 'Player 1',
          score: 100
        },
        {
          playerName: 'Player 2',
          score: 200
        }
      ])

      expect(result).toBeDefined()
      expect(result.scores).toHaveLength(2)
      expect(result.scores.map(x => x.player.name)).toContain('Player 1')
      expect(result.scores.map(x => x.player.name)).toContain('Player 2')
    })

    test('Only tournament members can create game results', async () => {
      await client.authenticate(testuser2.username, testuser2.password)

      expectGraphQlError(
        async () => await createGameresult(client, tournament.slug),
        GraphQlError.NOT_TOURNAMENT_MEMBER
      )
    })

    test('Should delete a game result', async () => {
      const { slug } = tournament

      const { id } = await createGameresult(client, tournament.slug)

      await deleteGameResult(client, id)

      const gameresult = await loadGameResult(client, id)

      expect(gameresult).toBeNull()
    })

    test('Should not allow deleting a game result for non-members', async () => {
      const { slug } = tournament

      const { id } = await createGameresult(client, tournament.slug)

      await client.authenticate(testuser2.username, testuser2.password)

      expectGraphQlError(
        async () => await deleteGameResult(client, id),
        GraphQlError.NOT_TOURNAMENT_MEMBER
      )
    })

    test('Only admins can delete game results', async () => {
      const { slug } = tournament

      const { id } = await createGameresult(client, tournament.slug)

      const { token } = await createInvitation(client, slug)

      await client.authenticate(testuser2.username, testuser2.password)

      await acceptInvitation(client, token)

      expectGraphQlError(
        async () => await deleteGameResult(client, id),
        GraphQlError.NOT_ALLOWED_BASED_ON_ROLE
      )
    })

    test('Admins can edit an existing game result', async () => {
      const { slug } = tournament

      const { id } = await createGameresult(client, tournament.slug)

      await updateGameResult(client, id, 120677, [
        {
          playerName: 'Player 1',
          score: 100
        },
        {
          playerName: 'Player 4',
          score: 133
        }
      ])

      const gameresult = await loadGameResult(client, id)

      expect(gameresult.bggInfo.bggid).toBe(120677)
      expect(gameresult.scores.length).toBe(2)
      expect(gameresult.scores.map(s => s.player.name)).toContain('Player 4')
    })
  })
})
