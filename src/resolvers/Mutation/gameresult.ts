import * as logger from 'winston'
import { GraphQLResolveInfo } from 'graphql'
import { Context } from '../../utils'
import { GameStats } from '../../services/game-stats'
import { BoardGameGeek } from '../../services/board-game-geek'
import {
  Gameresult,
  GameresultUpdateInput,
  GameresultWhereUniqueInput
} from '../../generated/prisma'

interface PlayerIdAndScore {
  score: number
  playerId: string
}

interface Score {
  score: number
  playerName: string
}

async function getPlayerIdsAndScore(
  slug: string,
  scores: Score[],
  ctx
): Promise<PlayerIdAndScore[]> {
  const players = await ctx.db.query.players({
    where: {
      AND: [
        {
          name_in: scores.map(x => x.playerName)
        },
        {
          tournament: {
            slug
          }
        }
      ]
    }
  })

  logger.debug('Found players', players)

  return await Promise.all(
    scores.map(
      async ({ playerName, score }): Promise<PlayerIdAndScore> => {
        let { id: playerId } = players.find(x => x.name === playerName) || {
          id: null
        }

        if (playerId === null) {
          logger.debug(
            `Player ${playerName} did not exist. Creating player in Prisma.`
          )

          const newPlayer = await ctx.db.mutation.createPlayer({
            data: {
              name: playerName,
              tournament: {
                connect: {
                  slug
                }
              }
            }
          })
          playerId = newPlayer.id
        }

        return {
          playerId,
          score
        }
      }
    )
  )
}

async function getGameWeightAndUpdateInfo(
  bggid: number,
  ctx: Context
): Promise<number> {
  // Load bgg info from prisma
  const bggInfo = await ctx.db.query.bggInfo(
    {
      where: {
        bggid
      }
    },
    `{
		weight
    lastReload
	}`
  )

  if (bggInfo === null) {
    logger.debug(
      `Game with bggid ${bggid} not found in Prisma. Loading it from boardgamegeek.`
    )

    const gameInfo = await BoardGameGeek.getGameInfo(bggid)
    await ctx.db.mutation.createBggInfo({
      data: {
        bggid,
        description: gameInfo.description,
        lastReload: gameInfo.lastReload,
        maxPlayers: gameInfo.maxPlayers,
        name: gameInfo.name,
        playTime: gameInfo.playTime,
        ownedBy: gameInfo.ownedBy,
        rank: gameInfo.rank,
        rating: gameInfo.rating,
        thumbnail: gameInfo.thumbnail,
        weight: gameInfo.weight,
        yearPublished: gameInfo.yearPublished
      }
    })

    return gameInfo.weight
  }

  const lastUpdatedDays = Math.ceil(
    (new Date().getTime() - new Date(bggInfo.lastReload).getTime()) /
      (1000 * 3600 * 24)
  )
  if (lastUpdatedDays > (process.env.BGG_INFO_MAX_AGE_DAYS || 30)) {
    logger.debug(
      `Last reload of game with bggid ${bggid} was ${lastUpdatedDays} ago. Reloading data from boardgamegeek.`
    )

    // BGG data needs update
    const gameInfo = await BoardGameGeek.getGameInfo(bggid)
    await ctx.db.mutation.updateBggInfo({
      where: {
        bggid
      },
      data: {
        description: gameInfo.description,
        lastReload: gameInfo.lastReload,
        maxPlayers: gameInfo.maxPlayers,
        name: gameInfo.name,
        playTime: gameInfo.playTime,
        ownedBy: gameInfo.ownedBy,
        rank: gameInfo.rank,
        rating: gameInfo.rating,
        thumbnail: gameInfo.thumbnail,
        weight: gameInfo.weight,
        yearPublished: gameInfo.yearPublished
      }
    })

    logger.debug(`Game with bggid ${bggid} was updated in Prisma`)

    return gameInfo.weight
  }

  logger.debug(
    `Game with bggid ${bggid} is up to date. Not reloading data from boardgamegeek.`
  )
  return bggInfo.weight
}

async function deleteDangnlingScoresAndPlayers(ctx: Context) {
  await ctx.db.mutation.deleteManyScores({ where: { gameresult: null } })
  await ctx.db.mutation.deleteManyPlayers({
    where: { scores_every: { gameresult: null } }
  })
}

export const gameresult = {
  async deleteGameresult(
    parent: any,
    args: any,
    ctx: Context,
    info: GraphQLResolveInfo
  ) {
    logger.info('Deleting game result', args)

    const { slug } = ctx.tournament
    const { id } = args.where

    const result = await ctx.db.mutation.deleteGameresult(args, info)
    await GameStats.deleteGameresult(slug, id)

    // delete dangling scores & players
    await deleteDangnlingScoresAndPlayers(ctx)

    return result
  },
  async updateGameresult(
    parent: any,
    args: { data: any; where: GameresultWhereUniqueInput },
    ctx: Context,
    info: GraphQLResolveInfo
  ) {
    const { slug } = ctx.tournament

    logger.info('Updating game result', args, ' of tournament', slug)

    // create player if it doesn't exist yet
    const playerIdsAndScore: PlayerIdAndScore[] = await getPlayerIdsAndScore(
      slug,
      args.data.scores,
      ctx
    )

    // delete scores currently connected to this game result
    await ctx.db.mutation.deleteManyScores({
      where: { gameresult: args.where }
    })

    // get weight and update bgg info if necessary
    const weight = await getGameWeightAndUpdateInfo(args.data.bggid, ctx)

    const gameresult = await ctx.db.mutation.updateGameresult(
      {
        where: args.where,
        data: {
          numPlayers: args.data.scores.length,
          tournament: {
            connect: {
              slug
            }
          },
          scores: {
            create: playerIdsAndScore.map(x => ({
              player: {
                connect: {
                  id: x.playerId
                }
              },
              score: x.score
            }))
          },
          bggInfo: {
            connect: {
              bggid: args.data.bggid
            }
          }
        }
      },
      `{
			id
			time
			bggInfo {
				weight
			}
		}`
    )

    logger.debug(`Game result ${gameresult.id} updated in Prisma`)

    try {
      // update the weight of the game
      await GameStats.setGameWeight(args.data.bggid.toString(), weight)
      logger.debug(
        `Game weight of game ${args.data.bggid} updated in game stats.`
      )

      // post gameresult to game stats
      await GameStats.updateGameresult(slug, {
        id: gameresult.id,
        bggid: args.data.bggid.toString(),
        time: gameresult.time.toString(),
        scores: playerIdsAndScore.map(x => ({
          player: x.playerId,
          score: x.score
        }))
      })
      logger.debug(`Game result ${gameresult.id} added to game stats`)
    } catch (err) {
      logger.error(`Failed to update game result: ${err.message}`)

      throw err
    }

    // delete dangling scores & players
    await deleteDangnlingScoresAndPlayers(ctx)

    return ctx.db.query.gameresult(
      {
        where: {
          id: gameresult.id
        }
      },
      info
    )
  },
  async createGameresult(
    parent: any,
    args: any,
    ctx: Context,
    info: GraphQLResolveInfo
  ) {
    logger.info('Adding new game result', args)

    // create player if it doesn't exist yet
    const playerIdsAndScore: PlayerIdAndScore[] = await getPlayerIdsAndScore(
      args.data.slug,
      args.data.scores,
      ctx
    )

    // get weight and update bgg info if necessary
    const weight = await getGameWeightAndUpdateInfo(args.data.bggid, ctx)

    const gameresult = await ctx.db.mutation.createGameresult(
      {
        data: {
          time: args.data.time || new Date(),
          numPlayers: args.data.scores.length,
          tournament: {
            connect: {
              slug: args.data.slug
            }
          },
          scores: {
            create: playerIdsAndScore.map(x => ({
              player: {
                connect: {
                  id: x.playerId
                }
              },
              score: x.score
            }))
          },
          bggInfo: {
            connect: {
              bggid: args.data.bggid
            }
          }
        }
      },
      `{
			id
			time
			bggInfo {
				weight
			}
		}`
    )

    logger.debug(`Game result ${gameresult.id} added to Prisma`)

    try {
      // update the weight of the game
      await GameStats.setGameWeight(args.data.bggid.toString(), weight)
      logger.debug(
        `Game weight of game ${args.data.bggid} updated in game stats.`
      )

      // post gameresult to game stats
      await GameStats.addGameresult(args.data.slug, {
        id: gameresult.id,
        bggid: args.data.bggid.toString(),
        time: gameresult.time.toString(),
        scores: playerIdsAndScore.map(x => ({
          player: x.playerId,
          score: x.score
        }))
      })
      logger.debug(`Game result ${gameresult.id} added to game stats`)
    } catch (err) {
      logger.error(`Failed to add game result: ${err.message}`)
      // revert gameresult if anything failed
      ctx.db.mutation.deleteGameresult({
        where: {
          id: gameresult.id
        }
      })

      throw err
    }

    return ctx.db.query.gameresult(
      {
        where: {
          id: gameresult.id
        }
      },
      info
    )
  }
}
