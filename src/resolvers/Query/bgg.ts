import { forwardTo } from 'prisma-binding'
import { Context } from '../../utils'
import { BoardGameGeek } from '../../services/board-game-geek'

export const bgg = {
  bggInfoes: forwardTo('db'),

  searchBggGame(parent, args) {
    return BoardGameGeek.searchGame(args.search)
  },

  async bggGameInfo(parent, args, ctx: Context, info) {
    if (args.where.id) {
      return ctx.db.query.bggInfo(args, info)
    }

    // load bggInfo from both prisma and BGG in parallel. If bggInfo did not exist in prisma we
    // will add it.
    const [gameInfo, bggInfoPrisma] = await Promise.all([
      BoardGameGeek.getGameInfo(args.where.bggid),
      ctx.db.query.bggInfo(args, info)
    ])

    if (!bggInfoPrisma) {
      // not yet saved in prisma -> create it
      return await ctx.db.mutation.createBggInfo(
        {
          data: {
            bggid: gameInfo.bggid,
            description: gameInfo.description,
            lastReload: gameInfo.lastReload,
            maxPlayers: gameInfo.maxPlayers,
            name: gameInfo.name,
            playTime: gameInfo.playTime,
            ownedBy: gameInfo.ownedBy,
            rank: gameInfo.rank,
            rating: gameInfo.rating,
            thumbnail: gameInfo.thumbnail,
            weight: gameInfo.weight,
            yearPublished: gameInfo.yearPublished
          }
        },
        info
      )
    }

    return bggInfoPrisma
  }
}
