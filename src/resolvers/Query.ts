import { getUserId, Context } from '../utils'
import { forwardTo } from 'prisma-binding'

import { bgg } from './Query/bgg'

export const Query = {
  me(parent, args, ctx: Context, info) {
    const { auth0id } = getUserId(ctx)
    return ctx.db.query.user({ where: { auth0id } }, info)
  },
  tournaments: forwardTo('db'),
  tournament: forwardTo('db'),
  invitation: forwardTo('db'),
  gameresults: forwardTo('db'),
  gameresult: forwardTo('db'),

  ...bgg
}
