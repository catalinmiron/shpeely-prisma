import { GraphQLResolveInfo, GraphQLSchema } from 'graphql'
import { IResolvers } from 'graphql-tools/dist/Interfaces'
import { Options } from 'graphql-binding'
import { makePrismaBindingClass, BasePrismaOptions } from 'prisma-binding'

export interface Query {
  tournamentMembers: <T = TournamentMember[]>(
    args: {
      where?: TournamentMemberWhereInput
      orderBy?: TournamentMemberOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  bggInfoes: <T = BggInfo[]>(
    args: {
      where?: BggInfoWhereInput
      orderBy?: BggInfoOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  files: <T = File[]>(
    args: {
      where?: FileWhereInput
      orderBy?: FileOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  gameresults: <T = Gameresult[]>(
    args: {
      where?: GameresultWhereInput
      orderBy?: GameresultOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  players: <T = Player[]>(
    args: {
      where?: PlayerWhereInput
      orderBy?: PlayerOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  scores: <T = Score[]>(
    args: {
      where?: ScoreWhereInput
      orderBy?: ScoreOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  tournaments: <T = Tournament[]>(
    args: {
      where?: TournamentWhereInput
      orderBy?: TournamentOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  invitations: <T = Invitation[]>(
    args: {
      where?: InvitationWhereInput
      orderBy?: InvitationOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  users: <T = User[]>(
    args: {
      where?: UserWhereInput
      orderBy?: UserOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  tournamentMember: <T = TournamentMember | null>(
    args: { where: TournamentMemberWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  bggInfo: <T = BggInfo | null>(
    args: { where: BggInfoWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  file: <T = File | null>(
    args: { where: FileWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  gameresult: <T = Gameresult | null>(
    args: { where: GameresultWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  player: <T = Player | null>(
    args: { where: PlayerWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  score: <T = Score | null>(
    args: { where: ScoreWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  tournament: <T = Tournament | null>(
    args: { where: TournamentWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  invitation: <T = Invitation | null>(
    args: { where: InvitationWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  user: <T = User | null>(
    args: { where: UserWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  tournamentMembersConnection: <T = TournamentMemberConnection>(
    args: {
      where?: TournamentMemberWhereInput
      orderBy?: TournamentMemberOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  bggInfoesConnection: <T = BggInfoConnection>(
    args: {
      where?: BggInfoWhereInput
      orderBy?: BggInfoOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  filesConnection: <T = FileConnection>(
    args: {
      where?: FileWhereInput
      orderBy?: FileOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  gameresultsConnection: <T = GameresultConnection>(
    args: {
      where?: GameresultWhereInput
      orderBy?: GameresultOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  playersConnection: <T = PlayerConnection>(
    args: {
      where?: PlayerWhereInput
      orderBy?: PlayerOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  scoresConnection: <T = ScoreConnection>(
    args: {
      where?: ScoreWhereInput
      orderBy?: ScoreOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  tournamentsConnection: <T = TournamentConnection>(
    args: {
      where?: TournamentWhereInput
      orderBy?: TournamentOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  invitationsConnection: <T = InvitationConnection>(
    args: {
      where?: InvitationWhereInput
      orderBy?: InvitationOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  usersConnection: <T = UserConnection>(
    args: {
      where?: UserWhereInput
      orderBy?: UserOrderByInput
      skip?: Int
      after?: String
      before?: String
      first?: Int
      last?: Int
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  node: <T = Node | null>(
    args: { id: ID_Output },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
}

export interface Mutation {
  createTournamentMember: <T = TournamentMember>(
    args: { data: TournamentMemberCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createBggInfo: <T = BggInfo>(
    args: { data: BggInfoCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createFile: <T = File>(
    args: { data: FileCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createGameresult: <T = Gameresult>(
    args: { data: GameresultCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createPlayer: <T = Player>(
    args: { data: PlayerCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createScore: <T = Score>(
    args: { data: ScoreCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createTournament: <T = Tournament>(
    args: { data: TournamentCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createInvitation: <T = Invitation>(
    args: { data: InvitationCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  createUser: <T = User>(
    args: { data: UserCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateTournamentMember: <T = TournamentMember | null>(
    args: {
      data: TournamentMemberUpdateInput
      where: TournamentMemberWhereUniqueInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateBggInfo: <T = BggInfo | null>(
    args: { data: BggInfoUpdateInput; where: BggInfoWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateFile: <T = File | null>(
    args: { data: FileUpdateInput; where: FileWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateGameresult: <T = Gameresult | null>(
    args: { data: GameresultUpdateInput; where: GameresultWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updatePlayer: <T = Player | null>(
    args: { data: PlayerUpdateInput; where: PlayerWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateScore: <T = Score | null>(
    args: { data: ScoreUpdateInput; where: ScoreWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateTournament: <T = Tournament | null>(
    args: { data: TournamentUpdateInput; where: TournamentWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateInvitation: <T = Invitation | null>(
    args: { data: InvitationUpdateInput; where: InvitationWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateUser: <T = User | null>(
    args: { data: UserUpdateInput; where: UserWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteTournamentMember: <T = TournamentMember | null>(
    args: { where: TournamentMemberWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteBggInfo: <T = BggInfo | null>(
    args: { where: BggInfoWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteFile: <T = File | null>(
    args: { where: FileWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteGameresult: <T = Gameresult | null>(
    args: { where: GameresultWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deletePlayer: <T = Player | null>(
    args: { where: PlayerWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteScore: <T = Score | null>(
    args: { where: ScoreWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteTournament: <T = Tournament | null>(
    args: { where: TournamentWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteInvitation: <T = Invitation | null>(
    args: { where: InvitationWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteUser: <T = User | null>(
    args: { where: UserWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertTournamentMember: <T = TournamentMember>(
    args: {
      where: TournamentMemberWhereUniqueInput
      create: TournamentMemberCreateInput
      update: TournamentMemberUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertBggInfo: <T = BggInfo>(
    args: {
      where: BggInfoWhereUniqueInput
      create: BggInfoCreateInput
      update: BggInfoUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertFile: <T = File>(
    args: {
      where: FileWhereUniqueInput
      create: FileCreateInput
      update: FileUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertGameresult: <T = Gameresult>(
    args: {
      where: GameresultWhereUniqueInput
      create: GameresultCreateInput
      update: GameresultUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertPlayer: <T = Player>(
    args: {
      where: PlayerWhereUniqueInput
      create: PlayerCreateInput
      update: PlayerUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertScore: <T = Score>(
    args: {
      where: ScoreWhereUniqueInput
      create: ScoreCreateInput
      update: ScoreUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertTournament: <T = Tournament>(
    args: {
      where: TournamentWhereUniqueInput
      create: TournamentCreateInput
      update: TournamentUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertInvitation: <T = Invitation>(
    args: {
      where: InvitationWhereUniqueInput
      create: InvitationCreateInput
      update: InvitationUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  upsertUser: <T = User>(
    args: {
      where: UserWhereUniqueInput
      create: UserCreateInput
      update: UserUpdateInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyTournamentMembers: <T = BatchPayload>(
    args: {
      data: TournamentMemberUpdateManyMutationInput
      where?: TournamentMemberWhereInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyBggInfoes: <T = BatchPayload>(
    args: { data: BggInfoUpdateManyMutationInput; where?: BggInfoWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyFiles: <T = BatchPayload>(
    args: { data: FileUpdateManyMutationInput; where?: FileWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyGameresults: <T = BatchPayload>(
    args: {
      data: GameresultUpdateManyMutationInput
      where?: GameresultWhereInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyPlayers: <T = BatchPayload>(
    args: { data: PlayerUpdateManyMutationInput; where?: PlayerWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyScores: <T = BatchPayload>(
    args: { data: ScoreUpdateManyMutationInput; where?: ScoreWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyTournaments: <T = BatchPayload>(
    args: {
      data: TournamentUpdateManyMutationInput
      where?: TournamentWhereInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyInvitations: <T = BatchPayload>(
    args: {
      data: InvitationUpdateManyMutationInput
      where?: InvitationWhereInput
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  updateManyUsers: <T = BatchPayload>(
    args: { data: UserUpdateManyMutationInput; where?: UserWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyTournamentMembers: <T = BatchPayload>(
    args: { where?: TournamentMemberWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyBggInfoes: <T = BatchPayload>(
    args: { where?: BggInfoWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyFiles: <T = BatchPayload>(
    args: { where?: FileWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyGameresults: <T = BatchPayload>(
    args: { where?: GameresultWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyPlayers: <T = BatchPayload>(
    args: { where?: PlayerWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyScores: <T = BatchPayload>(
    args: { where?: ScoreWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyTournaments: <T = BatchPayload>(
    args: { where?: TournamentWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyInvitations: <T = BatchPayload>(
    args: { where?: InvitationWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
  deleteManyUsers: <T = BatchPayload>(
    args: { where?: UserWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>
}

export interface Subscription {
  tournamentMember: <T = TournamentMemberSubscriptionPayload | null>(
    args: { where?: TournamentMemberSubscriptionWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T>>
  bggInfo: <T = BggInfoSubscriptionPayload | null>(
    args: { where?: BggInfoSubscriptionWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T>>
  file: <T = FileSubscriptionPayload | null>(
    args: { where?: FileSubscriptionWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T>>
  gameresult: <T = GameresultSubscriptionPayload | null>(
    args: { where?: GameresultSubscriptionWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T>>
  player: <T = PlayerSubscriptionPayload | null>(
    args: { where?: PlayerSubscriptionWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T>>
  score: <T = ScoreSubscriptionPayload | null>(
    args: { where?: ScoreSubscriptionWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T>>
  tournament: <T = TournamentSubscriptionPayload | null>(
    args: { where?: TournamentSubscriptionWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T>>
  invitation: <T = InvitationSubscriptionPayload | null>(
    args: { where?: InvitationSubscriptionWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T>>
  user: <T = UserSubscriptionPayload | null>(
    args: { where?: UserSubscriptionWhereInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T>>
}

export interface Exists {
  TournamentMember: (where?: TournamentMemberWhereInput) => Promise<boolean>
  BggInfo: (where?: BggInfoWhereInput) => Promise<boolean>
  File: (where?: FileWhereInput) => Promise<boolean>
  Gameresult: (where?: GameresultWhereInput) => Promise<boolean>
  Player: (where?: PlayerWhereInput) => Promise<boolean>
  Score: (where?: ScoreWhereInput) => Promise<boolean>
  Tournament: (where?: TournamentWhereInput) => Promise<boolean>
  Invitation: (where?: InvitationWhereInput) => Promise<boolean>
  User: (where?: UserWhereInput) => Promise<boolean>
}

export interface Prisma {
  query: Query
  mutation: Mutation
  subscription: Subscription
  exists: Exists
  request: <T = any>(
    query: string,
    variables?: { [key: string]: any }
  ) => Promise<T>
  delegate(
    operation: 'query' | 'mutation',
    fieldName: string,
    args: {
      [key: string]: any
    },
    infoOrQuery?: GraphQLResolveInfo | string,
    options?: Options
  ): Promise<any>
  delegateSubscription(
    fieldName: string,
    args?: {
      [key: string]: any
    },
    infoOrQuery?: GraphQLResolveInfo | string,
    options?: Options
  ): Promise<AsyncIterator<any>>
  getAbstractResolvers(filterSchema?: GraphQLSchema | string): IResolvers
}

export interface BindingConstructor<T> {
  new (options: BasePrismaOptions): T
}
/**
 * Type Defs
 */

const typeDefs = `type AggregateBggInfo {
  count: Int!
}

type AggregateFile {
  count: Int!
}

type AggregateGameresult {
  count: Int!
}

type AggregateInvitation {
  count: Int!
}

type AggregatePlayer {
  count: Int!
}

type AggregateScore {
  count: Int!
}

type AggregateTournament {
  count: Int!
}

type AggregateTournamentMember {
  count: Int!
}

type AggregateUser {
  count: Int!
}

type BatchPayload {
  """The number of nodes that have been affected by the Batch operation."""
  count: Long!
}

type BggInfo implements Node {
  bggid: Int!
  createdAt: DateTime!
  description: String!
  gameresults(where: GameresultWhereInput, orderBy: GameresultOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Gameresult!]
  id: ID!
  maxPlayers: Int!
  name: String!
  ownedBy: Int!
  playTime: Int!
  rank: Int!
  rating: Float!
  thumbnail: String!
  updatedAt: DateTime!
  weight: Float!
  yearPublished: Int!
  lastReload: DateTime!
}

"""A connection to a list of items."""
type BggInfoConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [BggInfoEdge]!
  aggregate: AggregateBggInfo!
}

input BggInfoCreateInput {
  bggid: Int!
  description: String!
  maxPlayers: Int!
  name: String!
  ownedBy: Int!
  playTime: Int!
  rank: Int!
  rating: Float!
  thumbnail: String!
  weight: Float!
  yearPublished: Int!
  lastReload: DateTime
  gameresults: GameresultCreateManyWithoutBggInfoInput
}

input BggInfoCreateOneWithoutGameresultsInput {
  create: BggInfoCreateWithoutGameresultsInput
  connect: BggInfoWhereUniqueInput
}

input BggInfoCreateWithoutGameresultsInput {
  bggid: Int!
  description: String!
  maxPlayers: Int!
  name: String!
  ownedBy: Int!
  playTime: Int!
  rank: Int!
  rating: Float!
  thumbnail: String!
  weight: Float!
  yearPublished: Int!
  lastReload: DateTime
}

"""An edge in a connection."""
type BggInfoEdge {
  """The item at the end of the edge."""
  node: BggInfo!

  """A cursor for use in pagination."""
  cursor: String!
}

enum BggInfoOrderByInput {
  bggid_ASC
  bggid_DESC
  createdAt_ASC
  createdAt_DESC
  description_ASC
  description_DESC
  id_ASC
  id_DESC
  maxPlayers_ASC
  maxPlayers_DESC
  name_ASC
  name_DESC
  ownedBy_ASC
  ownedBy_DESC
  playTime_ASC
  playTime_DESC
  rank_ASC
  rank_DESC
  rating_ASC
  rating_DESC
  thumbnail_ASC
  thumbnail_DESC
  updatedAt_ASC
  updatedAt_DESC
  weight_ASC
  weight_DESC
  yearPublished_ASC
  yearPublished_DESC
  lastReload_ASC
  lastReload_DESC
}

type BggInfoPreviousValues {
  bggid: Int!
  createdAt: DateTime!
  description: String!
  id: ID!
  maxPlayers: Int!
  name: String!
  ownedBy: Int!
  playTime: Int!
  rank: Int!
  rating: Float!
  thumbnail: String!
  updatedAt: DateTime!
  weight: Float!
  yearPublished: Int!
  lastReload: DateTime!
}

type BggInfoSubscriptionPayload {
  mutation: MutationType!
  node: BggInfo
  updatedFields: [String!]
  previousValues: BggInfoPreviousValues
}

input BggInfoSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [BggInfoSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [BggInfoSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [BggInfoSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: BggInfoWhereInput
}

input BggInfoUpdateInput {
  bggid: Int
  description: String
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  weight: Float
  yearPublished: Int
  lastReload: DateTime
  gameresults: GameresultUpdateManyWithoutBggInfoInput
}

input BggInfoUpdateManyMutationInput {
  bggid: Int
  description: String
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  weight: Float
  yearPublished: Int
  lastReload: DateTime
}

input BggInfoUpdateOneWithoutGameresultsInput {
  create: BggInfoCreateWithoutGameresultsInput
  connect: BggInfoWhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: BggInfoUpdateWithoutGameresultsDataInput
  upsert: BggInfoUpsertWithoutGameresultsInput
}

input BggInfoUpdateWithoutGameresultsDataInput {
  bggid: Int
  description: String
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  weight: Float
  yearPublished: Int
  lastReload: DateTime
}

input BggInfoUpsertWithoutGameresultsInput {
  update: BggInfoUpdateWithoutGameresultsDataInput!
  create: BggInfoCreateWithoutGameresultsInput!
}

input BggInfoWhereInput {
  """Logical AND on all given filters."""
  AND: [BggInfoWhereInput!]

  """Logical OR on all given filters."""
  OR: [BggInfoWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [BggInfoWhereInput!]
  bggid: Int

  """All values that are not equal to given value."""
  bggid_not: Int

  """All values that are contained in given list."""
  bggid_in: [Int!]

  """All values that are not contained in given list."""
  bggid_not_in: [Int!]

  """All values less than the given value."""
  bggid_lt: Int

  """All values less than or equal the given value."""
  bggid_lte: Int

  """All values greater than the given value."""
  bggid_gt: Int

  """All values greater than or equal the given value."""
  bggid_gte: Int
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  description: String

  """All values that are not equal to given value."""
  description_not: String

  """All values that are contained in given list."""
  description_in: [String!]

  """All values that are not contained in given list."""
  description_not_in: [String!]

  """All values less than the given value."""
  description_lt: String

  """All values less than or equal the given value."""
  description_lte: String

  """All values greater than the given value."""
  description_gt: String

  """All values greater than or equal the given value."""
  description_gte: String

  """All values containing the given string."""
  description_contains: String

  """All values not containing the given string."""
  description_not_contains: String

  """All values starting with the given string."""
  description_starts_with: String

  """All values not starting with the given string."""
  description_not_starts_with: String

  """All values ending with the given string."""
  description_ends_with: String

  """All values not ending with the given string."""
  description_not_ends_with: String
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  maxPlayers: Int

  """All values that are not equal to given value."""
  maxPlayers_not: Int

  """All values that are contained in given list."""
  maxPlayers_in: [Int!]

  """All values that are not contained in given list."""
  maxPlayers_not_in: [Int!]

  """All values less than the given value."""
  maxPlayers_lt: Int

  """All values less than or equal the given value."""
  maxPlayers_lte: Int

  """All values greater than the given value."""
  maxPlayers_gt: Int

  """All values greater than or equal the given value."""
  maxPlayers_gte: Int
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  ownedBy: Int

  """All values that are not equal to given value."""
  ownedBy_not: Int

  """All values that are contained in given list."""
  ownedBy_in: [Int!]

  """All values that are not contained in given list."""
  ownedBy_not_in: [Int!]

  """All values less than the given value."""
  ownedBy_lt: Int

  """All values less than or equal the given value."""
  ownedBy_lte: Int

  """All values greater than the given value."""
  ownedBy_gt: Int

  """All values greater than or equal the given value."""
  ownedBy_gte: Int
  playTime: Int

  """All values that are not equal to given value."""
  playTime_not: Int

  """All values that are contained in given list."""
  playTime_in: [Int!]

  """All values that are not contained in given list."""
  playTime_not_in: [Int!]

  """All values less than the given value."""
  playTime_lt: Int

  """All values less than or equal the given value."""
  playTime_lte: Int

  """All values greater than the given value."""
  playTime_gt: Int

  """All values greater than or equal the given value."""
  playTime_gte: Int
  rank: Int

  """All values that are not equal to given value."""
  rank_not: Int

  """All values that are contained in given list."""
  rank_in: [Int!]

  """All values that are not contained in given list."""
  rank_not_in: [Int!]

  """All values less than the given value."""
  rank_lt: Int

  """All values less than or equal the given value."""
  rank_lte: Int

  """All values greater than the given value."""
  rank_gt: Int

  """All values greater than or equal the given value."""
  rank_gte: Int
  rating: Float

  """All values that are not equal to given value."""
  rating_not: Float

  """All values that are contained in given list."""
  rating_in: [Float!]

  """All values that are not contained in given list."""
  rating_not_in: [Float!]

  """All values less than the given value."""
  rating_lt: Float

  """All values less than or equal the given value."""
  rating_lte: Float

  """All values greater than the given value."""
  rating_gt: Float

  """All values greater than or equal the given value."""
  rating_gte: Float
  thumbnail: String

  """All values that are not equal to given value."""
  thumbnail_not: String

  """All values that are contained in given list."""
  thumbnail_in: [String!]

  """All values that are not contained in given list."""
  thumbnail_not_in: [String!]

  """All values less than the given value."""
  thumbnail_lt: String

  """All values less than or equal the given value."""
  thumbnail_lte: String

  """All values greater than the given value."""
  thumbnail_gt: String

  """All values greater than or equal the given value."""
  thumbnail_gte: String

  """All values containing the given string."""
  thumbnail_contains: String

  """All values not containing the given string."""
  thumbnail_not_contains: String

  """All values starting with the given string."""
  thumbnail_starts_with: String

  """All values not starting with the given string."""
  thumbnail_not_starts_with: String

  """All values ending with the given string."""
  thumbnail_ends_with: String

  """All values not ending with the given string."""
  thumbnail_not_ends_with: String
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  weight: Float

  """All values that are not equal to given value."""
  weight_not: Float

  """All values that are contained in given list."""
  weight_in: [Float!]

  """All values that are not contained in given list."""
  weight_not_in: [Float!]

  """All values less than the given value."""
  weight_lt: Float

  """All values less than or equal the given value."""
  weight_lte: Float

  """All values greater than the given value."""
  weight_gt: Float

  """All values greater than or equal the given value."""
  weight_gte: Float
  yearPublished: Int

  """All values that are not equal to given value."""
  yearPublished_not: Int

  """All values that are contained in given list."""
  yearPublished_in: [Int!]

  """All values that are not contained in given list."""
  yearPublished_not_in: [Int!]

  """All values less than the given value."""
  yearPublished_lt: Int

  """All values less than or equal the given value."""
  yearPublished_lte: Int

  """All values greater than the given value."""
  yearPublished_gt: Int

  """All values greater than or equal the given value."""
  yearPublished_gte: Int
  lastReload: DateTime

  """All values that are not equal to given value."""
  lastReload_not: DateTime

  """All values that are contained in given list."""
  lastReload_in: [DateTime!]

  """All values that are not contained in given list."""
  lastReload_not_in: [DateTime!]

  """All values less than the given value."""
  lastReload_lt: DateTime

  """All values less than or equal the given value."""
  lastReload_lte: DateTime

  """All values greater than the given value."""
  lastReload_gt: DateTime

  """All values greater than or equal the given value."""
  lastReload_gte: DateTime
  gameresults_every: GameresultWhereInput
  gameresults_some: GameresultWhereInput
  gameresults_none: GameresultWhereInput
}

input BggInfoWhereUniqueInput {
  bggid: Int
  id: ID
}

scalar DateTime

type File implements Node {
  contentType: String!
  createdAt: DateTime!
  id: ID!
  name: String!
  secret: String!
  size: Int!
  updatedAt: DateTime!
  url: String!
}

"""A connection to a list of items."""
type FileConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [FileEdge]!
  aggregate: AggregateFile!
}

input FileCreateInput {
  contentType: String!
  name: String!
  secret: String!
  size: Int!
  url: String!
}

"""An edge in a connection."""
type FileEdge {
  """The item at the end of the edge."""
  node: File!

  """A cursor for use in pagination."""
  cursor: String!
}

enum FileOrderByInput {
  contentType_ASC
  contentType_DESC
  createdAt_ASC
  createdAt_DESC
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  secret_ASC
  secret_DESC
  size_ASC
  size_DESC
  updatedAt_ASC
  updatedAt_DESC
  url_ASC
  url_DESC
}

type FilePreviousValues {
  contentType: String!
  createdAt: DateTime!
  id: ID!
  name: String!
  secret: String!
  size: Int!
  updatedAt: DateTime!
  url: String!
}

type FileSubscriptionPayload {
  mutation: MutationType!
  node: File
  updatedFields: [String!]
  previousValues: FilePreviousValues
}

input FileSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [FileSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [FileSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [FileSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: FileWhereInput
}

input FileUpdateInput {
  contentType: String
  name: String
  secret: String
  size: Int
  url: String
}

input FileUpdateManyMutationInput {
  contentType: String
  name: String
  secret: String
  size: Int
  url: String
}

input FileWhereInput {
  """Logical AND on all given filters."""
  AND: [FileWhereInput!]

  """Logical OR on all given filters."""
  OR: [FileWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [FileWhereInput!]
  contentType: String

  """All values that are not equal to given value."""
  contentType_not: String

  """All values that are contained in given list."""
  contentType_in: [String!]

  """All values that are not contained in given list."""
  contentType_not_in: [String!]

  """All values less than the given value."""
  contentType_lt: String

  """All values less than or equal the given value."""
  contentType_lte: String

  """All values greater than the given value."""
  contentType_gt: String

  """All values greater than or equal the given value."""
  contentType_gte: String

  """All values containing the given string."""
  contentType_contains: String

  """All values not containing the given string."""
  contentType_not_contains: String

  """All values starting with the given string."""
  contentType_starts_with: String

  """All values not starting with the given string."""
  contentType_not_starts_with: String

  """All values ending with the given string."""
  contentType_ends_with: String

  """All values not ending with the given string."""
  contentType_not_ends_with: String
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  secret: String

  """All values that are not equal to given value."""
  secret_not: String

  """All values that are contained in given list."""
  secret_in: [String!]

  """All values that are not contained in given list."""
  secret_not_in: [String!]

  """All values less than the given value."""
  secret_lt: String

  """All values less than or equal the given value."""
  secret_lte: String

  """All values greater than the given value."""
  secret_gt: String

  """All values greater than or equal the given value."""
  secret_gte: String

  """All values containing the given string."""
  secret_contains: String

  """All values not containing the given string."""
  secret_not_contains: String

  """All values starting with the given string."""
  secret_starts_with: String

  """All values not starting with the given string."""
  secret_not_starts_with: String

  """All values ending with the given string."""
  secret_ends_with: String

  """All values not ending with the given string."""
  secret_not_ends_with: String
  size: Int

  """All values that are not equal to given value."""
  size_not: Int

  """All values that are contained in given list."""
  size_in: [Int!]

  """All values that are not contained in given list."""
  size_not_in: [Int!]

  """All values less than the given value."""
  size_lt: Int

  """All values less than or equal the given value."""
  size_lte: Int

  """All values greater than the given value."""
  size_gt: Int

  """All values greater than or equal the given value."""
  size_gte: Int
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  url: String

  """All values that are not equal to given value."""
  url_not: String

  """All values that are contained in given list."""
  url_in: [String!]

  """All values that are not contained in given list."""
  url_not_in: [String!]

  """All values less than the given value."""
  url_lt: String

  """All values less than or equal the given value."""
  url_lte: String

  """All values greater than the given value."""
  url_gt: String

  """All values greater than or equal the given value."""
  url_gte: String

  """All values containing the given string."""
  url_contains: String

  """All values not containing the given string."""
  url_not_contains: String

  """All values starting with the given string."""
  url_starts_with: String

  """All values not starting with the given string."""
  url_not_starts_with: String

  """All values ending with the given string."""
  url_ends_with: String

  """All values not ending with the given string."""
  url_not_ends_with: String
}

input FileWhereUniqueInput {
  id: ID
  secret: String
  url: String
}

type Gameresult implements Node {
  bggInfo: BggInfo
  createdAt: DateTime!
  id: ID!
  numPlayers: Int
  scores(where: ScoreWhereInput, orderBy: ScoreOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Score!]
  time: DateTime!
  tournament: Tournament!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type GameresultConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [GameresultEdge]!
  aggregate: AggregateGameresult!
}

input GameresultCreateInput {
  numPlayers: Int
  time: DateTime!
  bggInfo: BggInfoCreateOneWithoutGameresultsInput
  scores: ScoreCreateManyWithoutGameresultInput
  tournament: TournamentCreateOneWithoutGameresultsInput!
}

input GameresultCreateManyWithoutBggInfoInput {
  create: [GameresultCreateWithoutBggInfoInput!]
  connect: [GameresultWhereUniqueInput!]
}

input GameresultCreateManyWithoutTournamentInput {
  create: [GameresultCreateWithoutTournamentInput!]
  connect: [GameresultWhereUniqueInput!]
}

input GameresultCreateOneWithoutScoresInput {
  create: GameresultCreateWithoutScoresInput
  connect: GameresultWhereUniqueInput
}

input GameresultCreateWithoutBggInfoInput {
  numPlayers: Int
  time: DateTime!
  scores: ScoreCreateManyWithoutGameresultInput
  tournament: TournamentCreateOneWithoutGameresultsInput!
}

input GameresultCreateWithoutScoresInput {
  numPlayers: Int
  time: DateTime!
  bggInfo: BggInfoCreateOneWithoutGameresultsInput
  tournament: TournamentCreateOneWithoutGameresultsInput!
}

input GameresultCreateWithoutTournamentInput {
  numPlayers: Int
  time: DateTime!
  bggInfo: BggInfoCreateOneWithoutGameresultsInput
  scores: ScoreCreateManyWithoutGameresultInput
}

"""An edge in a connection."""
type GameresultEdge {
  """The item at the end of the edge."""
  node: Gameresult!

  """A cursor for use in pagination."""
  cursor: String!
}

enum GameresultOrderByInput {
  createdAt_ASC
  createdAt_DESC
  id_ASC
  id_DESC
  numPlayers_ASC
  numPlayers_DESC
  time_ASC
  time_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type GameresultPreviousValues {
  createdAt: DateTime!
  id: ID!
  numPlayers: Int
  time: DateTime!
  updatedAt: DateTime!
}

input GameresultScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [GameresultScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [GameresultScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [GameresultScalarWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  numPlayers: Int

  """All values that are not equal to given value."""
  numPlayers_not: Int

  """All values that are contained in given list."""
  numPlayers_in: [Int!]

  """All values that are not contained in given list."""
  numPlayers_not_in: [Int!]

  """All values less than the given value."""
  numPlayers_lt: Int

  """All values less than or equal the given value."""
  numPlayers_lte: Int

  """All values greater than the given value."""
  numPlayers_gt: Int

  """All values greater than or equal the given value."""
  numPlayers_gte: Int
  time: DateTime

  """All values that are not equal to given value."""
  time_not: DateTime

  """All values that are contained in given list."""
  time_in: [DateTime!]

  """All values that are not contained in given list."""
  time_not_in: [DateTime!]

  """All values less than the given value."""
  time_lt: DateTime

  """All values less than or equal the given value."""
  time_lte: DateTime

  """All values greater than the given value."""
  time_gt: DateTime

  """All values greater than or equal the given value."""
  time_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
}

type GameresultSubscriptionPayload {
  mutation: MutationType!
  node: Gameresult
  updatedFields: [String!]
  previousValues: GameresultPreviousValues
}

input GameresultSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [GameresultSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [GameresultSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [GameresultSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: GameresultWhereInput
}

input GameresultUpdateInput {
  numPlayers: Int
  time: DateTime
  bggInfo: BggInfoUpdateOneWithoutGameresultsInput
  scores: ScoreUpdateManyWithoutGameresultInput
  tournament: TournamentUpdateOneRequiredWithoutGameresultsInput
}

input GameresultUpdateManyDataInput {
  numPlayers: Int
  time: DateTime
}

input GameresultUpdateManyMutationInput {
  numPlayers: Int
  time: DateTime
}

input GameresultUpdateManyWithoutBggInfoInput {
  create: [GameresultCreateWithoutBggInfoInput!]
  connect: [GameresultWhereUniqueInput!]
  disconnect: [GameresultWhereUniqueInput!]
  delete: [GameresultWhereUniqueInput!]
  update: [GameresultUpdateWithWhereUniqueWithoutBggInfoInput!]
  updateMany: [GameresultUpdateManyWithWhereNestedInput!]
  deleteMany: [GameresultScalarWhereInput!]
  upsert: [GameresultUpsertWithWhereUniqueWithoutBggInfoInput!]
}

input GameresultUpdateManyWithoutTournamentInput {
  create: [GameresultCreateWithoutTournamentInput!]
  connect: [GameresultWhereUniqueInput!]
  disconnect: [GameresultWhereUniqueInput!]
  delete: [GameresultWhereUniqueInput!]
  update: [GameresultUpdateWithWhereUniqueWithoutTournamentInput!]
  updateMany: [GameresultUpdateManyWithWhereNestedInput!]
  deleteMany: [GameresultScalarWhereInput!]
  upsert: [GameresultUpsertWithWhereUniqueWithoutTournamentInput!]
}

input GameresultUpdateManyWithWhereNestedInput {
  where: GameresultScalarWhereInput!
  data: GameresultUpdateManyDataInput!
}

input GameresultUpdateOneWithoutScoresInput {
  create: GameresultCreateWithoutScoresInput
  connect: GameresultWhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: GameresultUpdateWithoutScoresDataInput
  upsert: GameresultUpsertWithoutScoresInput
}

input GameresultUpdateWithoutBggInfoDataInput {
  numPlayers: Int
  time: DateTime
  scores: ScoreUpdateManyWithoutGameresultInput
  tournament: TournamentUpdateOneRequiredWithoutGameresultsInput
}

input GameresultUpdateWithoutScoresDataInput {
  numPlayers: Int
  time: DateTime
  bggInfo: BggInfoUpdateOneWithoutGameresultsInput
  tournament: TournamentUpdateOneRequiredWithoutGameresultsInput
}

input GameresultUpdateWithoutTournamentDataInput {
  numPlayers: Int
  time: DateTime
  bggInfo: BggInfoUpdateOneWithoutGameresultsInput
  scores: ScoreUpdateManyWithoutGameresultInput
}

input GameresultUpdateWithWhereUniqueWithoutBggInfoInput {
  where: GameresultWhereUniqueInput!
  data: GameresultUpdateWithoutBggInfoDataInput!
}

input GameresultUpdateWithWhereUniqueWithoutTournamentInput {
  where: GameresultWhereUniqueInput!
  data: GameresultUpdateWithoutTournamentDataInput!
}

input GameresultUpsertWithoutScoresInput {
  update: GameresultUpdateWithoutScoresDataInput!
  create: GameresultCreateWithoutScoresInput!
}

input GameresultUpsertWithWhereUniqueWithoutBggInfoInput {
  where: GameresultWhereUniqueInput!
  update: GameresultUpdateWithoutBggInfoDataInput!
  create: GameresultCreateWithoutBggInfoInput!
}

input GameresultUpsertWithWhereUniqueWithoutTournamentInput {
  where: GameresultWhereUniqueInput!
  update: GameresultUpdateWithoutTournamentDataInput!
  create: GameresultCreateWithoutTournamentInput!
}

input GameresultWhereInput {
  """Logical AND on all given filters."""
  AND: [GameresultWhereInput!]

  """Logical OR on all given filters."""
  OR: [GameresultWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [GameresultWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  numPlayers: Int

  """All values that are not equal to given value."""
  numPlayers_not: Int

  """All values that are contained in given list."""
  numPlayers_in: [Int!]

  """All values that are not contained in given list."""
  numPlayers_not_in: [Int!]

  """All values less than the given value."""
  numPlayers_lt: Int

  """All values less than or equal the given value."""
  numPlayers_lte: Int

  """All values greater than the given value."""
  numPlayers_gt: Int

  """All values greater than or equal the given value."""
  numPlayers_gte: Int
  time: DateTime

  """All values that are not equal to given value."""
  time_not: DateTime

  """All values that are contained in given list."""
  time_in: [DateTime!]

  """All values that are not contained in given list."""
  time_not_in: [DateTime!]

  """All values less than the given value."""
  time_lt: DateTime

  """All values less than or equal the given value."""
  time_lte: DateTime

  """All values greater than the given value."""
  time_gt: DateTime

  """All values greater than or equal the given value."""
  time_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  bggInfo: BggInfoWhereInput
  scores_every: ScoreWhereInput
  scores_some: ScoreWhereInput
  scores_none: ScoreWhereInput
  tournament: TournamentWhereInput
}

input GameresultWhereUniqueInput {
  id: ID
}

type Invitation {
  token: String!
  state: InvitationState!
  email: String!
  inviter: User!
  tournament: Tournament!
  createdAt: DateTime!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type InvitationConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [InvitationEdge]!
  aggregate: AggregateInvitation!
}

input InvitationCreateInput {
  token: String!
  state: InvitationState
  email: String!
  inviter: UserCreateOneInput!
  tournament: TournamentCreateOneWithoutInvitationsInput!
}

input InvitationCreateManyWithoutTournamentInput {
  create: [InvitationCreateWithoutTournamentInput!]
  connect: [InvitationWhereUniqueInput!]
}

input InvitationCreateWithoutTournamentInput {
  token: String!
  state: InvitationState
  email: String!
  inviter: UserCreateOneInput!
}

"""An edge in a connection."""
type InvitationEdge {
  """The item at the end of the edge."""
  node: Invitation!

  """A cursor for use in pagination."""
  cursor: String!
}

enum InvitationOrderByInput {
  token_ASC
  token_DESC
  state_ASC
  state_DESC
  email_ASC
  email_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
  id_ASC
  id_DESC
}

type InvitationPreviousValues {
  token: String!
  state: InvitationState!
  email: String!
  createdAt: DateTime!
  updatedAt: DateTime!
}

input InvitationScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [InvitationScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [InvitationScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [InvitationScalarWhereInput!]
  token: String

  """All values that are not equal to given value."""
  token_not: String

  """All values that are contained in given list."""
  token_in: [String!]

  """All values that are not contained in given list."""
  token_not_in: [String!]

  """All values less than the given value."""
  token_lt: String

  """All values less than or equal the given value."""
  token_lte: String

  """All values greater than the given value."""
  token_gt: String

  """All values greater than or equal the given value."""
  token_gte: String

  """All values containing the given string."""
  token_contains: String

  """All values not containing the given string."""
  token_not_contains: String

  """All values starting with the given string."""
  token_starts_with: String

  """All values not starting with the given string."""
  token_not_starts_with: String

  """All values ending with the given string."""
  token_ends_with: String

  """All values not ending with the given string."""
  token_not_ends_with: String
  state: InvitationState

  """All values that are not equal to given value."""
  state_not: InvitationState

  """All values that are contained in given list."""
  state_in: [InvitationState!]

  """All values that are not contained in given list."""
  state_not_in: [InvitationState!]
  email: String

  """All values that are not equal to given value."""
  email_not: String

  """All values that are contained in given list."""
  email_in: [String!]

  """All values that are not contained in given list."""
  email_not_in: [String!]

  """All values less than the given value."""
  email_lt: String

  """All values less than or equal the given value."""
  email_lte: String

  """All values greater than the given value."""
  email_gt: String

  """All values greater than or equal the given value."""
  email_gte: String

  """All values containing the given string."""
  email_contains: String

  """All values not containing the given string."""
  email_not_contains: String

  """All values starting with the given string."""
  email_starts_with: String

  """All values not starting with the given string."""
  email_not_starts_with: String

  """All values ending with the given string."""
  email_ends_with: String

  """All values not ending with the given string."""
  email_not_ends_with: String
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
}

enum InvitationState {
  PENDING
  ACCEPTED
  REJECTED
}

type InvitationSubscriptionPayload {
  mutation: MutationType!
  node: Invitation
  updatedFields: [String!]
  previousValues: InvitationPreviousValues
}

input InvitationSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [InvitationSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [InvitationSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [InvitationSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: InvitationWhereInput
}

input InvitationUpdateInput {
  token: String
  state: InvitationState
  email: String
  inviter: UserUpdateOneRequiredInput
  tournament: TournamentUpdateOneRequiredWithoutInvitationsInput
}

input InvitationUpdateManyDataInput {
  token: String
  state: InvitationState
  email: String
}

input InvitationUpdateManyMutationInput {
  token: String
  state: InvitationState
  email: String
}

input InvitationUpdateManyWithoutTournamentInput {
  create: [InvitationCreateWithoutTournamentInput!]
  connect: [InvitationWhereUniqueInput!]
  disconnect: [InvitationWhereUniqueInput!]
  delete: [InvitationWhereUniqueInput!]
  update: [InvitationUpdateWithWhereUniqueWithoutTournamentInput!]
  updateMany: [InvitationUpdateManyWithWhereNestedInput!]
  deleteMany: [InvitationScalarWhereInput!]
  upsert: [InvitationUpsertWithWhereUniqueWithoutTournamentInput!]
}

input InvitationUpdateManyWithWhereNestedInput {
  where: InvitationScalarWhereInput!
  data: InvitationUpdateManyDataInput!
}

input InvitationUpdateWithoutTournamentDataInput {
  token: String
  state: InvitationState
  email: String
  inviter: UserUpdateOneRequiredInput
}

input InvitationUpdateWithWhereUniqueWithoutTournamentInput {
  where: InvitationWhereUniqueInput!
  data: InvitationUpdateWithoutTournamentDataInput!
}

input InvitationUpsertWithWhereUniqueWithoutTournamentInput {
  where: InvitationWhereUniqueInput!
  update: InvitationUpdateWithoutTournamentDataInput!
  create: InvitationCreateWithoutTournamentInput!
}

input InvitationWhereInput {
  """Logical AND on all given filters."""
  AND: [InvitationWhereInput!]

  """Logical OR on all given filters."""
  OR: [InvitationWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [InvitationWhereInput!]
  token: String

  """All values that are not equal to given value."""
  token_not: String

  """All values that are contained in given list."""
  token_in: [String!]

  """All values that are not contained in given list."""
  token_not_in: [String!]

  """All values less than the given value."""
  token_lt: String

  """All values less than or equal the given value."""
  token_lte: String

  """All values greater than the given value."""
  token_gt: String

  """All values greater than or equal the given value."""
  token_gte: String

  """All values containing the given string."""
  token_contains: String

  """All values not containing the given string."""
  token_not_contains: String

  """All values starting with the given string."""
  token_starts_with: String

  """All values not starting with the given string."""
  token_not_starts_with: String

  """All values ending with the given string."""
  token_ends_with: String

  """All values not ending with the given string."""
  token_not_ends_with: String
  state: InvitationState

  """All values that are not equal to given value."""
  state_not: InvitationState

  """All values that are contained in given list."""
  state_in: [InvitationState!]

  """All values that are not contained in given list."""
  state_not_in: [InvitationState!]
  email: String

  """All values that are not equal to given value."""
  email_not: String

  """All values that are contained in given list."""
  email_in: [String!]

  """All values that are not contained in given list."""
  email_not_in: [String!]

  """All values less than the given value."""
  email_lt: String

  """All values less than or equal the given value."""
  email_lte: String

  """All values greater than the given value."""
  email_gt: String

  """All values greater than or equal the given value."""
  email_gte: String

  """All values containing the given string."""
  email_contains: String

  """All values not containing the given string."""
  email_not_contains: String

  """All values starting with the given string."""
  email_starts_with: String

  """All values not starting with the given string."""
  email_not_starts_with: String

  """All values ending with the given string."""
  email_ends_with: String

  """All values not ending with the given string."""
  email_not_ends_with: String
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  inviter: UserWhereInput
  tournament: TournamentWhereInput
}

input InvitationWhereUniqueInput {
  token: String
}

"""
The \`Long\` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
"""
scalar Long

type Mutation {
  createTournamentMember(data: TournamentMemberCreateInput!): TournamentMember!
  createBggInfo(data: BggInfoCreateInput!): BggInfo!
  createFile(data: FileCreateInput!): File!
  createGameresult(data: GameresultCreateInput!): Gameresult!
  createPlayer(data: PlayerCreateInput!): Player!
  createScore(data: ScoreCreateInput!): Score!
  createTournament(data: TournamentCreateInput!): Tournament!
  createInvitation(data: InvitationCreateInput!): Invitation!
  createUser(data: UserCreateInput!): User!
  updateTournamentMember(data: TournamentMemberUpdateInput!, where: TournamentMemberWhereUniqueInput!): TournamentMember
  updateBggInfo(data: BggInfoUpdateInput!, where: BggInfoWhereUniqueInput!): BggInfo
  updateFile(data: FileUpdateInput!, where: FileWhereUniqueInput!): File
  updateGameresult(data: GameresultUpdateInput!, where: GameresultWhereUniqueInput!): Gameresult
  updatePlayer(data: PlayerUpdateInput!, where: PlayerWhereUniqueInput!): Player
  updateScore(data: ScoreUpdateInput!, where: ScoreWhereUniqueInput!): Score
  updateTournament(data: TournamentUpdateInput!, where: TournamentWhereUniqueInput!): Tournament
  updateInvitation(data: InvitationUpdateInput!, where: InvitationWhereUniqueInput!): Invitation
  updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
  deleteTournamentMember(where: TournamentMemberWhereUniqueInput!): TournamentMember
  deleteBggInfo(where: BggInfoWhereUniqueInput!): BggInfo
  deleteFile(where: FileWhereUniqueInput!): File
  deleteGameresult(where: GameresultWhereUniqueInput!): Gameresult
  deletePlayer(where: PlayerWhereUniqueInput!): Player
  deleteScore(where: ScoreWhereUniqueInput!): Score
  deleteTournament(where: TournamentWhereUniqueInput!): Tournament
  deleteInvitation(where: InvitationWhereUniqueInput!): Invitation
  deleteUser(where: UserWhereUniqueInput!): User
  upsertTournamentMember(where: TournamentMemberWhereUniqueInput!, create: TournamentMemberCreateInput!, update: TournamentMemberUpdateInput!): TournamentMember!
  upsertBggInfo(where: BggInfoWhereUniqueInput!, create: BggInfoCreateInput!, update: BggInfoUpdateInput!): BggInfo!
  upsertFile(where: FileWhereUniqueInput!, create: FileCreateInput!, update: FileUpdateInput!): File!
  upsertGameresult(where: GameresultWhereUniqueInput!, create: GameresultCreateInput!, update: GameresultUpdateInput!): Gameresult!
  upsertPlayer(where: PlayerWhereUniqueInput!, create: PlayerCreateInput!, update: PlayerUpdateInput!): Player!
  upsertScore(where: ScoreWhereUniqueInput!, create: ScoreCreateInput!, update: ScoreUpdateInput!): Score!
  upsertTournament(where: TournamentWhereUniqueInput!, create: TournamentCreateInput!, update: TournamentUpdateInput!): Tournament!
  upsertInvitation(where: InvitationWhereUniqueInput!, create: InvitationCreateInput!, update: InvitationUpdateInput!): Invitation!
  upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
  updateManyTournamentMembers(data: TournamentMemberUpdateManyMutationInput!, where: TournamentMemberWhereInput): BatchPayload!
  updateManyBggInfoes(data: BggInfoUpdateManyMutationInput!, where: BggInfoWhereInput): BatchPayload!
  updateManyFiles(data: FileUpdateManyMutationInput!, where: FileWhereInput): BatchPayload!
  updateManyGameresults(data: GameresultUpdateManyMutationInput!, where: GameresultWhereInput): BatchPayload!
  updateManyPlayers(data: PlayerUpdateManyMutationInput!, where: PlayerWhereInput): BatchPayload!
  updateManyScores(data: ScoreUpdateManyMutationInput!, where: ScoreWhereInput): BatchPayload!
  updateManyTournaments(data: TournamentUpdateManyMutationInput!, where: TournamentWhereInput): BatchPayload!
  updateManyInvitations(data: InvitationUpdateManyMutationInput!, where: InvitationWhereInput): BatchPayload!
  updateManyUsers(data: UserUpdateManyMutationInput!, where: UserWhereInput): BatchPayload!
  deleteManyTournamentMembers(where: TournamentMemberWhereInput): BatchPayload!
  deleteManyBggInfoes(where: BggInfoWhereInput): BatchPayload!
  deleteManyFiles(where: FileWhereInput): BatchPayload!
  deleteManyGameresults(where: GameresultWhereInput): BatchPayload!
  deleteManyPlayers(where: PlayerWhereInput): BatchPayload!
  deleteManyScores(where: ScoreWhereInput): BatchPayload!
  deleteManyTournaments(where: TournamentWhereInput): BatchPayload!
  deleteManyInvitations(where: InvitationWhereInput): BatchPayload!
  deleteManyUsers(where: UserWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

"""An object with an ID"""
interface Node {
  """The id of the object."""
  id: ID!
}

"""Information about pagination in a connection."""
type PageInfo {
  """When paginating forwards, are there more items?"""
  hasNextPage: Boolean!

  """When paginating backwards, are there more items?"""
  hasPreviousPage: Boolean!

  """When paginating backwards, the cursor to continue."""
  startCursor: String

  """When paginating forwards, the cursor to continue."""
  endCursor: String
}

type Player implements Node {
  createdAt: DateTime!
  id: ID!
  name: String!
  scores(where: ScoreWhereInput, orderBy: ScoreOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Score!]
  tournament: Tournament!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type PlayerConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [PlayerEdge]!
  aggregate: AggregatePlayer!
}

input PlayerCreateInput {
  name: String!
  scores: ScoreCreateManyWithoutPlayerInput
  tournament: TournamentCreateOneWithoutPlayersInput!
}

input PlayerCreateManyWithoutTournamentInput {
  create: [PlayerCreateWithoutTournamentInput!]
  connect: [PlayerWhereUniqueInput!]
}

input PlayerCreateOneWithoutScoresInput {
  create: PlayerCreateWithoutScoresInput
  connect: PlayerWhereUniqueInput
}

input PlayerCreateWithoutScoresInput {
  name: String!
  tournament: TournamentCreateOneWithoutPlayersInput!
}

input PlayerCreateWithoutTournamentInput {
  name: String!
  scores: ScoreCreateManyWithoutPlayerInput
}

"""An edge in a connection."""
type PlayerEdge {
  """The item at the end of the edge."""
  node: Player!

  """A cursor for use in pagination."""
  cursor: String!
}

enum PlayerOrderByInput {
  createdAt_ASC
  createdAt_DESC
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type PlayerPreviousValues {
  createdAt: DateTime!
  id: ID!
  name: String!
  updatedAt: DateTime!
}

input PlayerScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [PlayerScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [PlayerScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PlayerScalarWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
}

type PlayerSubscriptionPayload {
  mutation: MutationType!
  node: Player
  updatedFields: [String!]
  previousValues: PlayerPreviousValues
}

input PlayerSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [PlayerSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [PlayerSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PlayerSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: PlayerWhereInput
}

input PlayerUpdateInput {
  name: String
  scores: ScoreUpdateManyWithoutPlayerInput
  tournament: TournamentUpdateOneRequiredWithoutPlayersInput
}

input PlayerUpdateManyDataInput {
  name: String
}

input PlayerUpdateManyMutationInput {
  name: String
}

input PlayerUpdateManyWithoutTournamentInput {
  create: [PlayerCreateWithoutTournamentInput!]
  connect: [PlayerWhereUniqueInput!]
  disconnect: [PlayerWhereUniqueInput!]
  delete: [PlayerWhereUniqueInput!]
  update: [PlayerUpdateWithWhereUniqueWithoutTournamentInput!]
  updateMany: [PlayerUpdateManyWithWhereNestedInput!]
  deleteMany: [PlayerScalarWhereInput!]
  upsert: [PlayerUpsertWithWhereUniqueWithoutTournamentInput!]
}

input PlayerUpdateManyWithWhereNestedInput {
  where: PlayerScalarWhereInput!
  data: PlayerUpdateManyDataInput!
}

input PlayerUpdateOneRequiredWithoutScoresInput {
  create: PlayerCreateWithoutScoresInput
  connect: PlayerWhereUniqueInput
  update: PlayerUpdateWithoutScoresDataInput
  upsert: PlayerUpsertWithoutScoresInput
}

input PlayerUpdateWithoutScoresDataInput {
  name: String
  tournament: TournamentUpdateOneRequiredWithoutPlayersInput
}

input PlayerUpdateWithoutTournamentDataInput {
  name: String
  scores: ScoreUpdateManyWithoutPlayerInput
}

input PlayerUpdateWithWhereUniqueWithoutTournamentInput {
  where: PlayerWhereUniqueInput!
  data: PlayerUpdateWithoutTournamentDataInput!
}

input PlayerUpsertWithoutScoresInput {
  update: PlayerUpdateWithoutScoresDataInput!
  create: PlayerCreateWithoutScoresInput!
}

input PlayerUpsertWithWhereUniqueWithoutTournamentInput {
  where: PlayerWhereUniqueInput!
  update: PlayerUpdateWithoutTournamentDataInput!
  create: PlayerCreateWithoutTournamentInput!
}

input PlayerWhereInput {
  """Logical AND on all given filters."""
  AND: [PlayerWhereInput!]

  """Logical OR on all given filters."""
  OR: [PlayerWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PlayerWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  scores_every: ScoreWhereInput
  scores_some: ScoreWhereInput
  scores_none: ScoreWhereInput
  tournament: TournamentWhereInput
}

input PlayerWhereUniqueInput {
  id: ID
}

type Query {
  tournamentMembers(where: TournamentMemberWhereInput, orderBy: TournamentMemberOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [TournamentMember]!
  bggInfoes(where: BggInfoWhereInput, orderBy: BggInfoOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [BggInfo]!
  files(where: FileWhereInput, orderBy: FileOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [File]!
  gameresults(where: GameresultWhereInput, orderBy: GameresultOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Gameresult]!
  players(where: PlayerWhereInput, orderBy: PlayerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Player]!
  scores(where: ScoreWhereInput, orderBy: ScoreOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Score]!
  tournaments(where: TournamentWhereInput, orderBy: TournamentOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Tournament]!
  invitations(where: InvitationWhereInput, orderBy: InvitationOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Invitation]!
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
  tournamentMember(where: TournamentMemberWhereUniqueInput!): TournamentMember
  bggInfo(where: BggInfoWhereUniqueInput!): BggInfo
  file(where: FileWhereUniqueInput!): File
  gameresult(where: GameresultWhereUniqueInput!): Gameresult
  player(where: PlayerWhereUniqueInput!): Player
  score(where: ScoreWhereUniqueInput!): Score
  tournament(where: TournamentWhereUniqueInput!): Tournament
  invitation(where: InvitationWhereUniqueInput!): Invitation
  user(where: UserWhereUniqueInput!): User
  tournamentMembersConnection(where: TournamentMemberWhereInput, orderBy: TournamentMemberOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): TournamentMemberConnection!
  bggInfoesConnection(where: BggInfoWhereInput, orderBy: BggInfoOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): BggInfoConnection!
  filesConnection(where: FileWhereInput, orderBy: FileOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): FileConnection!
  gameresultsConnection(where: GameresultWhereInput, orderBy: GameresultOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): GameresultConnection!
  playersConnection(where: PlayerWhereInput, orderBy: PlayerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PlayerConnection!
  scoresConnection(where: ScoreWhereInput, orderBy: ScoreOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): ScoreConnection!
  tournamentsConnection(where: TournamentWhereInput, orderBy: TournamentOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): TournamentConnection!
  invitationsConnection(where: InvitationWhereInput, orderBy: InvitationOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): InvitationConnection!
  usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!

  """Fetches an object given its ID"""
  node(
    """The ID of an object"""
    id: ID!
  ): Node
}

type Score implements Node {
  createdAt: DateTime!
  gameresult: Gameresult
  id: ID!
  player: Player!
  score: Int!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type ScoreConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [ScoreEdge]!
  aggregate: AggregateScore!
}

input ScoreCreateInput {
  score: Int!
  gameresult: GameresultCreateOneWithoutScoresInput
  player: PlayerCreateOneWithoutScoresInput!
}

input ScoreCreateManyWithoutGameresultInput {
  create: [ScoreCreateWithoutGameresultInput!]
  connect: [ScoreWhereUniqueInput!]
}

input ScoreCreateManyWithoutPlayerInput {
  create: [ScoreCreateWithoutPlayerInput!]
  connect: [ScoreWhereUniqueInput!]
}

input ScoreCreateWithoutGameresultInput {
  score: Int!
  player: PlayerCreateOneWithoutScoresInput!
}

input ScoreCreateWithoutPlayerInput {
  score: Int!
  gameresult: GameresultCreateOneWithoutScoresInput
}

"""An edge in a connection."""
type ScoreEdge {
  """The item at the end of the edge."""
  node: Score!

  """A cursor for use in pagination."""
  cursor: String!
}

enum ScoreOrderByInput {
  createdAt_ASC
  createdAt_DESC
  id_ASC
  id_DESC
  score_ASC
  score_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type ScorePreviousValues {
  createdAt: DateTime!
  id: ID!
  score: Int!
  updatedAt: DateTime!
}

input ScoreScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [ScoreScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [ScoreScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [ScoreScalarWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  score: Int

  """All values that are not equal to given value."""
  score_not: Int

  """All values that are contained in given list."""
  score_in: [Int!]

  """All values that are not contained in given list."""
  score_not_in: [Int!]

  """All values less than the given value."""
  score_lt: Int

  """All values less than or equal the given value."""
  score_lte: Int

  """All values greater than the given value."""
  score_gt: Int

  """All values greater than or equal the given value."""
  score_gte: Int
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
}

type ScoreSubscriptionPayload {
  mutation: MutationType!
  node: Score
  updatedFields: [String!]
  previousValues: ScorePreviousValues
}

input ScoreSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [ScoreSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [ScoreSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [ScoreSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: ScoreWhereInput
}

input ScoreUpdateInput {
  score: Int
  gameresult: GameresultUpdateOneWithoutScoresInput
  player: PlayerUpdateOneRequiredWithoutScoresInput
}

input ScoreUpdateManyDataInput {
  score: Int
}

input ScoreUpdateManyMutationInput {
  score: Int
}

input ScoreUpdateManyWithoutGameresultInput {
  create: [ScoreCreateWithoutGameresultInput!]
  connect: [ScoreWhereUniqueInput!]
  disconnect: [ScoreWhereUniqueInput!]
  delete: [ScoreWhereUniqueInput!]
  update: [ScoreUpdateWithWhereUniqueWithoutGameresultInput!]
  updateMany: [ScoreUpdateManyWithWhereNestedInput!]
  deleteMany: [ScoreScalarWhereInput!]
  upsert: [ScoreUpsertWithWhereUniqueWithoutGameresultInput!]
}

input ScoreUpdateManyWithoutPlayerInput {
  create: [ScoreCreateWithoutPlayerInput!]
  connect: [ScoreWhereUniqueInput!]
  disconnect: [ScoreWhereUniqueInput!]
  delete: [ScoreWhereUniqueInput!]
  update: [ScoreUpdateWithWhereUniqueWithoutPlayerInput!]
  updateMany: [ScoreUpdateManyWithWhereNestedInput!]
  deleteMany: [ScoreScalarWhereInput!]
  upsert: [ScoreUpsertWithWhereUniqueWithoutPlayerInput!]
}

input ScoreUpdateManyWithWhereNestedInput {
  where: ScoreScalarWhereInput!
  data: ScoreUpdateManyDataInput!
}

input ScoreUpdateWithoutGameresultDataInput {
  score: Int
  player: PlayerUpdateOneRequiredWithoutScoresInput
}

input ScoreUpdateWithoutPlayerDataInput {
  score: Int
  gameresult: GameresultUpdateOneWithoutScoresInput
}

input ScoreUpdateWithWhereUniqueWithoutGameresultInput {
  where: ScoreWhereUniqueInput!
  data: ScoreUpdateWithoutGameresultDataInput!
}

input ScoreUpdateWithWhereUniqueWithoutPlayerInput {
  where: ScoreWhereUniqueInput!
  data: ScoreUpdateWithoutPlayerDataInput!
}

input ScoreUpsertWithWhereUniqueWithoutGameresultInput {
  where: ScoreWhereUniqueInput!
  update: ScoreUpdateWithoutGameresultDataInput!
  create: ScoreCreateWithoutGameresultInput!
}

input ScoreUpsertWithWhereUniqueWithoutPlayerInput {
  where: ScoreWhereUniqueInput!
  update: ScoreUpdateWithoutPlayerDataInput!
  create: ScoreCreateWithoutPlayerInput!
}

input ScoreWhereInput {
  """Logical AND on all given filters."""
  AND: [ScoreWhereInput!]

  """Logical OR on all given filters."""
  OR: [ScoreWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [ScoreWhereInput!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  score: Int

  """All values that are not equal to given value."""
  score_not: Int

  """All values that are contained in given list."""
  score_in: [Int!]

  """All values that are not contained in given list."""
  score_not_in: [Int!]

  """All values less than the given value."""
  score_lt: Int

  """All values less than or equal the given value."""
  score_lte: Int

  """All values greater than the given value."""
  score_gt: Int

  """All values greater than or equal the given value."""
  score_gte: Int
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  gameresult: GameresultWhereInput
  player: PlayerWhereInput
}

input ScoreWhereUniqueInput {
  id: ID
}

type Subscription {
  tournamentMember(where: TournamentMemberSubscriptionWhereInput): TournamentMemberSubscriptionPayload
  bggInfo(where: BggInfoSubscriptionWhereInput): BggInfoSubscriptionPayload
  file(where: FileSubscriptionWhereInput): FileSubscriptionPayload
  gameresult(where: GameresultSubscriptionWhereInput): GameresultSubscriptionPayload
  player(where: PlayerSubscriptionWhereInput): PlayerSubscriptionPayload
  score(where: ScoreSubscriptionWhereInput): ScoreSubscriptionPayload
  tournament(where: TournamentSubscriptionWhereInput): TournamentSubscriptionPayload
  invitation(where: InvitationSubscriptionWhereInput): InvitationSubscriptionPayload
  user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
}

type Tournament {
  slug: String!
  gameresults(where: GameresultWhereInput, orderBy: GameresultOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Gameresult!]
  name: String!
  players(where: PlayerWhereInput, orderBy: PlayerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Player!]
  members(where: TournamentMemberWhereInput, orderBy: TournamentMemberOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [TournamentMember!]
  invitations(where: InvitationWhereInput, orderBy: InvitationOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Invitation!]
  createdAt: DateTime!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type TournamentConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [TournamentEdge]!
  aggregate: AggregateTournament!
}

input TournamentCreateInput {
  slug: String!
  name: String!
  gameresults: GameresultCreateManyWithoutTournamentInput
  players: PlayerCreateManyWithoutTournamentInput
  members: TournamentMemberCreateManyWithoutTournamentInput
  invitations: InvitationCreateManyWithoutTournamentInput
}

input TournamentCreateOneWithoutGameresultsInput {
  create: TournamentCreateWithoutGameresultsInput
  connect: TournamentWhereUniqueInput
}

input TournamentCreateOneWithoutInvitationsInput {
  create: TournamentCreateWithoutInvitationsInput
  connect: TournamentWhereUniqueInput
}

input TournamentCreateOneWithoutMembersInput {
  create: TournamentCreateWithoutMembersInput
  connect: TournamentWhereUniqueInput
}

input TournamentCreateOneWithoutPlayersInput {
  create: TournamentCreateWithoutPlayersInput
  connect: TournamentWhereUniqueInput
}

input TournamentCreateWithoutGameresultsInput {
  slug: String!
  name: String!
  players: PlayerCreateManyWithoutTournamentInput
  members: TournamentMemberCreateManyWithoutTournamentInput
  invitations: InvitationCreateManyWithoutTournamentInput
}

input TournamentCreateWithoutInvitationsInput {
  slug: String!
  name: String!
  gameresults: GameresultCreateManyWithoutTournamentInput
  players: PlayerCreateManyWithoutTournamentInput
  members: TournamentMemberCreateManyWithoutTournamentInput
}

input TournamentCreateWithoutMembersInput {
  slug: String!
  name: String!
  gameresults: GameresultCreateManyWithoutTournamentInput
  players: PlayerCreateManyWithoutTournamentInput
  invitations: InvitationCreateManyWithoutTournamentInput
}

input TournamentCreateWithoutPlayersInput {
  slug: String!
  name: String!
  gameresults: GameresultCreateManyWithoutTournamentInput
  members: TournamentMemberCreateManyWithoutTournamentInput
  invitations: InvitationCreateManyWithoutTournamentInput
}

"""An edge in a connection."""
type TournamentEdge {
  """The item at the end of the edge."""
  node: Tournament!

  """A cursor for use in pagination."""
  cursor: String!
}

type TournamentMember implements Node {
  id: ID!
  tournament: Tournament!
  user: User!
  role: TournamentMemberRole!
}

"""A connection to a list of items."""
type TournamentMemberConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [TournamentMemberEdge]!
  aggregate: AggregateTournamentMember!
}

input TournamentMemberCreateInput {
  role: TournamentMemberRole
  tournament: TournamentCreateOneWithoutMembersInput!
  user: UserCreateOneWithoutTournamentsInput!
}

input TournamentMemberCreateManyWithoutTournamentInput {
  create: [TournamentMemberCreateWithoutTournamentInput!]
  connect: [TournamentMemberWhereUniqueInput!]
}

input TournamentMemberCreateManyWithoutUserInput {
  create: [TournamentMemberCreateWithoutUserInput!]
  connect: [TournamentMemberWhereUniqueInput!]
}

input TournamentMemberCreateWithoutTournamentInput {
  role: TournamentMemberRole
  user: UserCreateOneWithoutTournamentsInput!
}

input TournamentMemberCreateWithoutUserInput {
  role: TournamentMemberRole
  tournament: TournamentCreateOneWithoutMembersInput!
}

"""An edge in a connection."""
type TournamentMemberEdge {
  """The item at the end of the edge."""
  node: TournamentMember!

  """A cursor for use in pagination."""
  cursor: String!
}

enum TournamentMemberOrderByInput {
  id_ASC
  id_DESC
  role_ASC
  role_DESC
  updatedAt_ASC
  updatedAt_DESC
  createdAt_ASC
  createdAt_DESC
}

type TournamentMemberPreviousValues {
  id: ID!
  role: TournamentMemberRole!
}

enum TournamentMemberRole {
  ADMIN
  MEMBER
}

input TournamentMemberScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [TournamentMemberScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [TournamentMemberScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TournamentMemberScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  role: TournamentMemberRole

  """All values that are not equal to given value."""
  role_not: TournamentMemberRole

  """All values that are contained in given list."""
  role_in: [TournamentMemberRole!]

  """All values that are not contained in given list."""
  role_not_in: [TournamentMemberRole!]
}

type TournamentMemberSubscriptionPayload {
  mutation: MutationType!
  node: TournamentMember
  updatedFields: [String!]
  previousValues: TournamentMemberPreviousValues
}

input TournamentMemberSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [TournamentMemberSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [TournamentMemberSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TournamentMemberSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: TournamentMemberWhereInput
}

input TournamentMemberUpdateInput {
  role: TournamentMemberRole
  tournament: TournamentUpdateOneRequiredWithoutMembersInput
  user: UserUpdateOneRequiredWithoutTournamentsInput
}

input TournamentMemberUpdateManyDataInput {
  role: TournamentMemberRole
}

input TournamentMemberUpdateManyMutationInput {
  role: TournamentMemberRole
}

input TournamentMemberUpdateManyWithoutTournamentInput {
  create: [TournamentMemberCreateWithoutTournamentInput!]
  connect: [TournamentMemberWhereUniqueInput!]
  disconnect: [TournamentMemberWhereUniqueInput!]
  delete: [TournamentMemberWhereUniqueInput!]
  update: [TournamentMemberUpdateWithWhereUniqueWithoutTournamentInput!]
  updateMany: [TournamentMemberUpdateManyWithWhereNestedInput!]
  deleteMany: [TournamentMemberScalarWhereInput!]
  upsert: [TournamentMemberUpsertWithWhereUniqueWithoutTournamentInput!]
}

input TournamentMemberUpdateManyWithoutUserInput {
  create: [TournamentMemberCreateWithoutUserInput!]
  connect: [TournamentMemberWhereUniqueInput!]
  disconnect: [TournamentMemberWhereUniqueInput!]
  delete: [TournamentMemberWhereUniqueInput!]
  update: [TournamentMemberUpdateWithWhereUniqueWithoutUserInput!]
  updateMany: [TournamentMemberUpdateManyWithWhereNestedInput!]
  deleteMany: [TournamentMemberScalarWhereInput!]
  upsert: [TournamentMemberUpsertWithWhereUniqueWithoutUserInput!]
}

input TournamentMemberUpdateManyWithWhereNestedInput {
  where: TournamentMemberScalarWhereInput!
  data: TournamentMemberUpdateManyDataInput!
}

input TournamentMemberUpdateWithoutTournamentDataInput {
  role: TournamentMemberRole
  user: UserUpdateOneRequiredWithoutTournamentsInput
}

input TournamentMemberUpdateWithoutUserDataInput {
  role: TournamentMemberRole
  tournament: TournamentUpdateOneRequiredWithoutMembersInput
}

input TournamentMemberUpdateWithWhereUniqueWithoutTournamentInput {
  where: TournamentMemberWhereUniqueInput!
  data: TournamentMemberUpdateWithoutTournamentDataInput!
}

input TournamentMemberUpdateWithWhereUniqueWithoutUserInput {
  where: TournamentMemberWhereUniqueInput!
  data: TournamentMemberUpdateWithoutUserDataInput!
}

input TournamentMemberUpsertWithWhereUniqueWithoutTournamentInput {
  where: TournamentMemberWhereUniqueInput!
  update: TournamentMemberUpdateWithoutTournamentDataInput!
  create: TournamentMemberCreateWithoutTournamentInput!
}

input TournamentMemberUpsertWithWhereUniqueWithoutUserInput {
  where: TournamentMemberWhereUniqueInput!
  update: TournamentMemberUpdateWithoutUserDataInput!
  create: TournamentMemberCreateWithoutUserInput!
}

input TournamentMemberWhereInput {
  """Logical AND on all given filters."""
  AND: [TournamentMemberWhereInput!]

  """Logical OR on all given filters."""
  OR: [TournamentMemberWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TournamentMemberWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  role: TournamentMemberRole

  """All values that are not equal to given value."""
  role_not: TournamentMemberRole

  """All values that are contained in given list."""
  role_in: [TournamentMemberRole!]

  """All values that are not contained in given list."""
  role_not_in: [TournamentMemberRole!]
  tournament: TournamentWhereInput
  user: UserWhereInput
}

input TournamentMemberWhereUniqueInput {
  id: ID
}

enum TournamentOrderByInput {
  slug_ASC
  slug_DESC
  name_ASC
  name_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
  id_ASC
  id_DESC
}

type TournamentPreviousValues {
  slug: String!
  name: String!
  createdAt: DateTime!
  updatedAt: DateTime!
}

type TournamentSubscriptionPayload {
  mutation: MutationType!
  node: Tournament
  updatedFields: [String!]
  previousValues: TournamentPreviousValues
}

input TournamentSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [TournamentSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [TournamentSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TournamentSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: TournamentWhereInput
}

input TournamentUpdateInput {
  slug: String
  name: String
  gameresults: GameresultUpdateManyWithoutTournamentInput
  players: PlayerUpdateManyWithoutTournamentInput
  members: TournamentMemberUpdateManyWithoutTournamentInput
  invitations: InvitationUpdateManyWithoutTournamentInput
}

input TournamentUpdateManyMutationInput {
  slug: String
  name: String
}

input TournamentUpdateOneRequiredWithoutGameresultsInput {
  create: TournamentCreateWithoutGameresultsInput
  connect: TournamentWhereUniqueInput
  update: TournamentUpdateWithoutGameresultsDataInput
  upsert: TournamentUpsertWithoutGameresultsInput
}

input TournamentUpdateOneRequiredWithoutInvitationsInput {
  create: TournamentCreateWithoutInvitationsInput
  connect: TournamentWhereUniqueInput
  update: TournamentUpdateWithoutInvitationsDataInput
  upsert: TournamentUpsertWithoutInvitationsInput
}

input TournamentUpdateOneRequiredWithoutMembersInput {
  create: TournamentCreateWithoutMembersInput
  connect: TournamentWhereUniqueInput
  update: TournamentUpdateWithoutMembersDataInput
  upsert: TournamentUpsertWithoutMembersInput
}

input TournamentUpdateOneRequiredWithoutPlayersInput {
  create: TournamentCreateWithoutPlayersInput
  connect: TournamentWhereUniqueInput
  update: TournamentUpdateWithoutPlayersDataInput
  upsert: TournamentUpsertWithoutPlayersInput
}

input TournamentUpdateWithoutGameresultsDataInput {
  slug: String
  name: String
  players: PlayerUpdateManyWithoutTournamentInput
  members: TournamentMemberUpdateManyWithoutTournamentInput
  invitations: InvitationUpdateManyWithoutTournamentInput
}

input TournamentUpdateWithoutInvitationsDataInput {
  slug: String
  name: String
  gameresults: GameresultUpdateManyWithoutTournamentInput
  players: PlayerUpdateManyWithoutTournamentInput
  members: TournamentMemberUpdateManyWithoutTournamentInput
}

input TournamentUpdateWithoutMembersDataInput {
  slug: String
  name: String
  gameresults: GameresultUpdateManyWithoutTournamentInput
  players: PlayerUpdateManyWithoutTournamentInput
  invitations: InvitationUpdateManyWithoutTournamentInput
}

input TournamentUpdateWithoutPlayersDataInput {
  slug: String
  name: String
  gameresults: GameresultUpdateManyWithoutTournamentInput
  members: TournamentMemberUpdateManyWithoutTournamentInput
  invitations: InvitationUpdateManyWithoutTournamentInput
}

input TournamentUpsertWithoutGameresultsInput {
  update: TournamentUpdateWithoutGameresultsDataInput!
  create: TournamentCreateWithoutGameresultsInput!
}

input TournamentUpsertWithoutInvitationsInput {
  update: TournamentUpdateWithoutInvitationsDataInput!
  create: TournamentCreateWithoutInvitationsInput!
}

input TournamentUpsertWithoutMembersInput {
  update: TournamentUpdateWithoutMembersDataInput!
  create: TournamentCreateWithoutMembersInput!
}

input TournamentUpsertWithoutPlayersInput {
  update: TournamentUpdateWithoutPlayersDataInput!
  create: TournamentCreateWithoutPlayersInput!
}

input TournamentWhereInput {
  """Logical AND on all given filters."""
  AND: [TournamentWhereInput!]

  """Logical OR on all given filters."""
  OR: [TournamentWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TournamentWhereInput!]
  slug: String

  """All values that are not equal to given value."""
  slug_not: String

  """All values that are contained in given list."""
  slug_in: [String!]

  """All values that are not contained in given list."""
  slug_not_in: [String!]

  """All values less than the given value."""
  slug_lt: String

  """All values less than or equal the given value."""
  slug_lte: String

  """All values greater than the given value."""
  slug_gt: String

  """All values greater than or equal the given value."""
  slug_gte: String

  """All values containing the given string."""
  slug_contains: String

  """All values not containing the given string."""
  slug_not_contains: String

  """All values starting with the given string."""
  slug_starts_with: String

  """All values not starting with the given string."""
  slug_not_starts_with: String

  """All values ending with the given string."""
  slug_ends_with: String

  """All values not ending with the given string."""
  slug_not_ends_with: String
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  gameresults_every: GameresultWhereInput
  gameresults_some: GameresultWhereInput
  gameresults_none: GameresultWhereInput
  players_every: PlayerWhereInput
  players_some: PlayerWhereInput
  players_none: PlayerWhereInput
  members_every: TournamentMemberWhereInput
  members_some: TournamentMemberWhereInput
  members_none: TournamentMemberWhereInput
  invitations_every: InvitationWhereInput
  invitations_some: InvitationWhereInput
  invitations_none: InvitationWhereInput
}

input TournamentWhereUniqueInput {
  slug: String
}

type User {
  email: String!
  auth0id: String!
  username: String!
  avatar: String!
  tournaments(where: TournamentMemberWhereInput, orderBy: TournamentMemberOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [TournamentMember!]
}

"""A connection to a list of items."""
type UserConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [UserEdge]!
  aggregate: AggregateUser!
}

input UserCreateInput {
  email: String!
  auth0id: String!
  username: String!
  avatar: String!
  tournaments: TournamentMemberCreateManyWithoutUserInput
}

input UserCreateOneInput {
  create: UserCreateInput
  connect: UserWhereUniqueInput
}

input UserCreateOneWithoutTournamentsInput {
  create: UserCreateWithoutTournamentsInput
  connect: UserWhereUniqueInput
}

input UserCreateWithoutTournamentsInput {
  email: String!
  auth0id: String!
  username: String!
  avatar: String!
}

"""An edge in a connection."""
type UserEdge {
  """The item at the end of the edge."""
  node: User!

  """A cursor for use in pagination."""
  cursor: String!
}

enum UserOrderByInput {
  email_ASC
  email_DESC
  auth0id_ASC
  auth0id_DESC
  username_ASC
  username_DESC
  avatar_ASC
  avatar_DESC
  id_ASC
  id_DESC
  updatedAt_ASC
  updatedAt_DESC
  createdAt_ASC
  createdAt_DESC
}

type UserPreviousValues {
  email: String!
  auth0id: String!
  username: String!
  avatar: String!
}

type UserSubscriptionPayload {
  mutation: MutationType!
  node: User
  updatedFields: [String!]
  previousValues: UserPreviousValues
}

input UserSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [UserSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: UserWhereInput
}

input UserUpdateDataInput {
  email: String
  auth0id: String
  username: String
  avatar: String
  tournaments: TournamentMemberUpdateManyWithoutUserInput
}

input UserUpdateInput {
  email: String
  auth0id: String
  username: String
  avatar: String
  tournaments: TournamentMemberUpdateManyWithoutUserInput
}

input UserUpdateManyMutationInput {
  email: String
  auth0id: String
  username: String
  avatar: String
}

input UserUpdateOneRequiredInput {
  create: UserCreateInput
  connect: UserWhereUniqueInput
  update: UserUpdateDataInput
  upsert: UserUpsertNestedInput
}

input UserUpdateOneRequiredWithoutTournamentsInput {
  create: UserCreateWithoutTournamentsInput
  connect: UserWhereUniqueInput
  update: UserUpdateWithoutTournamentsDataInput
  upsert: UserUpsertWithoutTournamentsInput
}

input UserUpdateWithoutTournamentsDataInput {
  email: String
  auth0id: String
  username: String
  avatar: String
}

input UserUpsertNestedInput {
  update: UserUpdateDataInput!
  create: UserCreateInput!
}

input UserUpsertWithoutTournamentsInput {
  update: UserUpdateWithoutTournamentsDataInput!
  create: UserCreateWithoutTournamentsInput!
}

input UserWhereInput {
  """Logical AND on all given filters."""
  AND: [UserWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserWhereInput!]
  email: String

  """All values that are not equal to given value."""
  email_not: String

  """All values that are contained in given list."""
  email_in: [String!]

  """All values that are not contained in given list."""
  email_not_in: [String!]

  """All values less than the given value."""
  email_lt: String

  """All values less than or equal the given value."""
  email_lte: String

  """All values greater than the given value."""
  email_gt: String

  """All values greater than or equal the given value."""
  email_gte: String

  """All values containing the given string."""
  email_contains: String

  """All values not containing the given string."""
  email_not_contains: String

  """All values starting with the given string."""
  email_starts_with: String

  """All values not starting with the given string."""
  email_not_starts_with: String

  """All values ending with the given string."""
  email_ends_with: String

  """All values not ending with the given string."""
  email_not_ends_with: String
  auth0id: String

  """All values that are not equal to given value."""
  auth0id_not: String

  """All values that are contained in given list."""
  auth0id_in: [String!]

  """All values that are not contained in given list."""
  auth0id_not_in: [String!]

  """All values less than the given value."""
  auth0id_lt: String

  """All values less than or equal the given value."""
  auth0id_lte: String

  """All values greater than the given value."""
  auth0id_gt: String

  """All values greater than or equal the given value."""
  auth0id_gte: String

  """All values containing the given string."""
  auth0id_contains: String

  """All values not containing the given string."""
  auth0id_not_contains: String

  """All values starting with the given string."""
  auth0id_starts_with: String

  """All values not starting with the given string."""
  auth0id_not_starts_with: String

  """All values ending with the given string."""
  auth0id_ends_with: String

  """All values not ending with the given string."""
  auth0id_not_ends_with: String
  username: String

  """All values that are not equal to given value."""
  username_not: String

  """All values that are contained in given list."""
  username_in: [String!]

  """All values that are not contained in given list."""
  username_not_in: [String!]

  """All values less than the given value."""
  username_lt: String

  """All values less than or equal the given value."""
  username_lte: String

  """All values greater than the given value."""
  username_gt: String

  """All values greater than or equal the given value."""
  username_gte: String

  """All values containing the given string."""
  username_contains: String

  """All values not containing the given string."""
  username_not_contains: String

  """All values starting with the given string."""
  username_starts_with: String

  """All values not starting with the given string."""
  username_not_starts_with: String

  """All values ending with the given string."""
  username_ends_with: String

  """All values not ending with the given string."""
  username_not_ends_with: String
  avatar: String

  """All values that are not equal to given value."""
  avatar_not: String

  """All values that are contained in given list."""
  avatar_in: [String!]

  """All values that are not contained in given list."""
  avatar_not_in: [String!]

  """All values less than the given value."""
  avatar_lt: String

  """All values less than or equal the given value."""
  avatar_lte: String

  """All values greater than the given value."""
  avatar_gt: String

  """All values greater than or equal the given value."""
  avatar_gte: String

  """All values containing the given string."""
  avatar_contains: String

  """All values not containing the given string."""
  avatar_not_contains: String

  """All values starting with the given string."""
  avatar_starts_with: String

  """All values not starting with the given string."""
  avatar_not_starts_with: String

  """All values ending with the given string."""
  avatar_ends_with: String

  """All values not ending with the given string."""
  avatar_not_ends_with: String
  tournaments_every: TournamentMemberWhereInput
  tournaments_some: TournamentMemberWhereInput
  tournaments_none: TournamentMemberWhereInput
}

input UserWhereUniqueInput {
  auth0id: String
}
`

export const Prisma = makePrismaBindingClass<BindingConstructor<Prisma>>({
  typeDefs
})

/**
 * Types
 */

export type TournamentMemberRole = 'ADMIN' | 'MEMBER'

export type InvitationState = 'PENDING' | 'ACCEPTED' | 'REJECTED'

export type TournamentMemberOrderByInput =
  | 'id_ASC'
  | 'id_DESC'
  | 'role_ASC'
  | 'role_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'

export type GameresultOrderByInput =
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'id_ASC'
  | 'id_DESC'
  | 'numPlayers_ASC'
  | 'numPlayers_DESC'
  | 'time_ASC'
  | 'time_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'

export type ScoreOrderByInput =
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'id_ASC'
  | 'id_DESC'
  | 'score_ASC'
  | 'score_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'

export type PlayerOrderByInput =
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'id_ASC'
  | 'id_DESC'
  | 'name_ASC'
  | 'name_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'

export type InvitationOrderByInput =
  | 'token_ASC'
  | 'token_DESC'
  | 'state_ASC'
  | 'state_DESC'
  | 'email_ASC'
  | 'email_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'
  | 'id_ASC'
  | 'id_DESC'

export type BggInfoOrderByInput =
  | 'bggid_ASC'
  | 'bggid_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'description_ASC'
  | 'description_DESC'
  | 'id_ASC'
  | 'id_DESC'
  | 'maxPlayers_ASC'
  | 'maxPlayers_DESC'
  | 'name_ASC'
  | 'name_DESC'
  | 'ownedBy_ASC'
  | 'ownedBy_DESC'
  | 'playTime_ASC'
  | 'playTime_DESC'
  | 'rank_ASC'
  | 'rank_DESC'
  | 'rating_ASC'
  | 'rating_DESC'
  | 'thumbnail_ASC'
  | 'thumbnail_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'
  | 'weight_ASC'
  | 'weight_DESC'
  | 'yearPublished_ASC'
  | 'yearPublished_DESC'
  | 'lastReload_ASC'
  | 'lastReload_DESC'

export type FileOrderByInput =
  | 'contentType_ASC'
  | 'contentType_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'id_ASC'
  | 'id_DESC'
  | 'name_ASC'
  | 'name_DESC'
  | 'secret_ASC'
  | 'secret_DESC'
  | 'size_ASC'
  | 'size_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'
  | 'url_ASC'
  | 'url_DESC'

export type TournamentOrderByInput =
  | 'slug_ASC'
  | 'slug_DESC'
  | 'name_ASC'
  | 'name_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'
  | 'id_ASC'
  | 'id_DESC'

export type UserOrderByInput =
  | 'email_ASC'
  | 'email_DESC'
  | 'auth0id_ASC'
  | 'auth0id_DESC'
  | 'username_ASC'
  | 'username_DESC'
  | 'avatar_ASC'
  | 'avatar_DESC'
  | 'id_ASC'
  | 'id_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'

export type MutationType = 'CREATED' | 'UPDATED' | 'DELETED'

export interface TournamentMemberWhereInput {
  AND?: TournamentMemberWhereInput[] | TournamentMemberWhereInput
  OR?: TournamentMemberWhereInput[] | TournamentMemberWhereInput
  NOT?: TournamentMemberWhereInput[] | TournamentMemberWhereInput
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  role?: TournamentMemberRole
  role_not?: TournamentMemberRole
  role_in?: TournamentMemberRole[] | TournamentMemberRole
  role_not_in?: TournamentMemberRole[] | TournamentMemberRole
  tournament?: TournamentWhereInput
  user?: UserWhereInput
}

export interface TournamentWhereInput {
  AND?: TournamentWhereInput[] | TournamentWhereInput
  OR?: TournamentWhereInput[] | TournamentWhereInput
  NOT?: TournamentWhereInput[] | TournamentWhereInput
  slug?: String
  slug_not?: String
  slug_in?: String[] | String
  slug_not_in?: String[] | String
  slug_lt?: String
  slug_lte?: String
  slug_gt?: String
  slug_gte?: String
  slug_contains?: String
  slug_not_contains?: String
  slug_starts_with?: String
  slug_not_starts_with?: String
  slug_ends_with?: String
  slug_not_ends_with?: String
  name?: String
  name_not?: String
  name_in?: String[] | String
  name_not_in?: String[] | String
  name_lt?: String
  name_lte?: String
  name_gt?: String
  name_gte?: String
  name_contains?: String
  name_not_contains?: String
  name_starts_with?: String
  name_not_starts_with?: String
  name_ends_with?: String
  name_not_ends_with?: String
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  updatedAt?: DateTime
  updatedAt_not?: DateTime
  updatedAt_in?: DateTime[] | DateTime
  updatedAt_not_in?: DateTime[] | DateTime
  updatedAt_lt?: DateTime
  updatedAt_lte?: DateTime
  updatedAt_gt?: DateTime
  updatedAt_gte?: DateTime
  gameresults_every?: GameresultWhereInput
  gameresults_some?: GameresultWhereInput
  gameresults_none?: GameresultWhereInput
  players_every?: PlayerWhereInput
  players_some?: PlayerWhereInput
  players_none?: PlayerWhereInput
  members_every?: TournamentMemberWhereInput
  members_some?: TournamentMemberWhereInput
  members_none?: TournamentMemberWhereInput
  invitations_every?: InvitationWhereInput
  invitations_some?: InvitationWhereInput
  invitations_none?: InvitationWhereInput
}

export interface GameresultWhereInput {
  AND?: GameresultWhereInput[] | GameresultWhereInput
  OR?: GameresultWhereInput[] | GameresultWhereInput
  NOT?: GameresultWhereInput[] | GameresultWhereInput
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  numPlayers?: Int
  numPlayers_not?: Int
  numPlayers_in?: Int[] | Int
  numPlayers_not_in?: Int[] | Int
  numPlayers_lt?: Int
  numPlayers_lte?: Int
  numPlayers_gt?: Int
  numPlayers_gte?: Int
  time?: DateTime
  time_not?: DateTime
  time_in?: DateTime[] | DateTime
  time_not_in?: DateTime[] | DateTime
  time_lt?: DateTime
  time_lte?: DateTime
  time_gt?: DateTime
  time_gte?: DateTime
  updatedAt?: DateTime
  updatedAt_not?: DateTime
  updatedAt_in?: DateTime[] | DateTime
  updatedAt_not_in?: DateTime[] | DateTime
  updatedAt_lt?: DateTime
  updatedAt_lte?: DateTime
  updatedAt_gt?: DateTime
  updatedAt_gte?: DateTime
  bggInfo?: BggInfoWhereInput
  scores_every?: ScoreWhereInput
  scores_some?: ScoreWhereInput
  scores_none?: ScoreWhereInput
  tournament?: TournamentWhereInput
}

export interface BggInfoWhereInput {
  AND?: BggInfoWhereInput[] | BggInfoWhereInput
  OR?: BggInfoWhereInput[] | BggInfoWhereInput
  NOT?: BggInfoWhereInput[] | BggInfoWhereInput
  bggid?: Int
  bggid_not?: Int
  bggid_in?: Int[] | Int
  bggid_not_in?: Int[] | Int
  bggid_lt?: Int
  bggid_lte?: Int
  bggid_gt?: Int
  bggid_gte?: Int
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  description?: String
  description_not?: String
  description_in?: String[] | String
  description_not_in?: String[] | String
  description_lt?: String
  description_lte?: String
  description_gt?: String
  description_gte?: String
  description_contains?: String
  description_not_contains?: String
  description_starts_with?: String
  description_not_starts_with?: String
  description_ends_with?: String
  description_not_ends_with?: String
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  maxPlayers?: Int
  maxPlayers_not?: Int
  maxPlayers_in?: Int[] | Int
  maxPlayers_not_in?: Int[] | Int
  maxPlayers_lt?: Int
  maxPlayers_lte?: Int
  maxPlayers_gt?: Int
  maxPlayers_gte?: Int
  name?: String
  name_not?: String
  name_in?: String[] | String
  name_not_in?: String[] | String
  name_lt?: String
  name_lte?: String
  name_gt?: String
  name_gte?: String
  name_contains?: String
  name_not_contains?: String
  name_starts_with?: String
  name_not_starts_with?: String
  name_ends_with?: String
  name_not_ends_with?: String
  ownedBy?: Int
  ownedBy_not?: Int
  ownedBy_in?: Int[] | Int
  ownedBy_not_in?: Int[] | Int
  ownedBy_lt?: Int
  ownedBy_lte?: Int
  ownedBy_gt?: Int
  ownedBy_gte?: Int
  playTime?: Int
  playTime_not?: Int
  playTime_in?: Int[] | Int
  playTime_not_in?: Int[] | Int
  playTime_lt?: Int
  playTime_lte?: Int
  playTime_gt?: Int
  playTime_gte?: Int
  rank?: Int
  rank_not?: Int
  rank_in?: Int[] | Int
  rank_not_in?: Int[] | Int
  rank_lt?: Int
  rank_lte?: Int
  rank_gt?: Int
  rank_gte?: Int
  rating?: Float
  rating_not?: Float
  rating_in?: Float[] | Float
  rating_not_in?: Float[] | Float
  rating_lt?: Float
  rating_lte?: Float
  rating_gt?: Float
  rating_gte?: Float
  thumbnail?: String
  thumbnail_not?: String
  thumbnail_in?: String[] | String
  thumbnail_not_in?: String[] | String
  thumbnail_lt?: String
  thumbnail_lte?: String
  thumbnail_gt?: String
  thumbnail_gte?: String
  thumbnail_contains?: String
  thumbnail_not_contains?: String
  thumbnail_starts_with?: String
  thumbnail_not_starts_with?: String
  thumbnail_ends_with?: String
  thumbnail_not_ends_with?: String
  updatedAt?: DateTime
  updatedAt_not?: DateTime
  updatedAt_in?: DateTime[] | DateTime
  updatedAt_not_in?: DateTime[] | DateTime
  updatedAt_lt?: DateTime
  updatedAt_lte?: DateTime
  updatedAt_gt?: DateTime
  updatedAt_gte?: DateTime
  weight?: Float
  weight_not?: Float
  weight_in?: Float[] | Float
  weight_not_in?: Float[] | Float
  weight_lt?: Float
  weight_lte?: Float
  weight_gt?: Float
  weight_gte?: Float
  yearPublished?: Int
  yearPublished_not?: Int
  yearPublished_in?: Int[] | Int
  yearPublished_not_in?: Int[] | Int
  yearPublished_lt?: Int
  yearPublished_lte?: Int
  yearPublished_gt?: Int
  yearPublished_gte?: Int
  lastReload?: DateTime
  lastReload_not?: DateTime
  lastReload_in?: DateTime[] | DateTime
  lastReload_not_in?: DateTime[] | DateTime
  lastReload_lt?: DateTime
  lastReload_lte?: DateTime
  lastReload_gt?: DateTime
  lastReload_gte?: DateTime
  gameresults_every?: GameresultWhereInput
  gameresults_some?: GameresultWhereInput
  gameresults_none?: GameresultWhereInput
}

export interface ScoreWhereInput {
  AND?: ScoreWhereInput[] | ScoreWhereInput
  OR?: ScoreWhereInput[] | ScoreWhereInput
  NOT?: ScoreWhereInput[] | ScoreWhereInput
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  score?: Int
  score_not?: Int
  score_in?: Int[] | Int
  score_not_in?: Int[] | Int
  score_lt?: Int
  score_lte?: Int
  score_gt?: Int
  score_gte?: Int
  updatedAt?: DateTime
  updatedAt_not?: DateTime
  updatedAt_in?: DateTime[] | DateTime
  updatedAt_not_in?: DateTime[] | DateTime
  updatedAt_lt?: DateTime
  updatedAt_lte?: DateTime
  updatedAt_gt?: DateTime
  updatedAt_gte?: DateTime
  gameresult?: GameresultWhereInput
  player?: PlayerWhereInput
}

export interface PlayerWhereInput {
  AND?: PlayerWhereInput[] | PlayerWhereInput
  OR?: PlayerWhereInput[] | PlayerWhereInput
  NOT?: PlayerWhereInput[] | PlayerWhereInput
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  name?: String
  name_not?: String
  name_in?: String[] | String
  name_not_in?: String[] | String
  name_lt?: String
  name_lte?: String
  name_gt?: String
  name_gte?: String
  name_contains?: String
  name_not_contains?: String
  name_starts_with?: String
  name_not_starts_with?: String
  name_ends_with?: String
  name_not_ends_with?: String
  updatedAt?: DateTime
  updatedAt_not?: DateTime
  updatedAt_in?: DateTime[] | DateTime
  updatedAt_not_in?: DateTime[] | DateTime
  updatedAt_lt?: DateTime
  updatedAt_lte?: DateTime
  updatedAt_gt?: DateTime
  updatedAt_gte?: DateTime
  scores_every?: ScoreWhereInput
  scores_some?: ScoreWhereInput
  scores_none?: ScoreWhereInput
  tournament?: TournamentWhereInput
}

export interface InvitationWhereInput {
  AND?: InvitationWhereInput[] | InvitationWhereInput
  OR?: InvitationWhereInput[] | InvitationWhereInput
  NOT?: InvitationWhereInput[] | InvitationWhereInput
  token?: String
  token_not?: String
  token_in?: String[] | String
  token_not_in?: String[] | String
  token_lt?: String
  token_lte?: String
  token_gt?: String
  token_gte?: String
  token_contains?: String
  token_not_contains?: String
  token_starts_with?: String
  token_not_starts_with?: String
  token_ends_with?: String
  token_not_ends_with?: String
  state?: InvitationState
  state_not?: InvitationState
  state_in?: InvitationState[] | InvitationState
  state_not_in?: InvitationState[] | InvitationState
  email?: String
  email_not?: String
  email_in?: String[] | String
  email_not_in?: String[] | String
  email_lt?: String
  email_lte?: String
  email_gt?: String
  email_gte?: String
  email_contains?: String
  email_not_contains?: String
  email_starts_with?: String
  email_not_starts_with?: String
  email_ends_with?: String
  email_not_ends_with?: String
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  updatedAt?: DateTime
  updatedAt_not?: DateTime
  updatedAt_in?: DateTime[] | DateTime
  updatedAt_not_in?: DateTime[] | DateTime
  updatedAt_lt?: DateTime
  updatedAt_lte?: DateTime
  updatedAt_gt?: DateTime
  updatedAt_gte?: DateTime
  inviter?: UserWhereInput
  tournament?: TournamentWhereInput
}

export interface UserWhereInput {
  AND?: UserWhereInput[] | UserWhereInput
  OR?: UserWhereInput[] | UserWhereInput
  NOT?: UserWhereInput[] | UserWhereInput
  email?: String
  email_not?: String
  email_in?: String[] | String
  email_not_in?: String[] | String
  email_lt?: String
  email_lte?: String
  email_gt?: String
  email_gte?: String
  email_contains?: String
  email_not_contains?: String
  email_starts_with?: String
  email_not_starts_with?: String
  email_ends_with?: String
  email_not_ends_with?: String
  auth0id?: String
  auth0id_not?: String
  auth0id_in?: String[] | String
  auth0id_not_in?: String[] | String
  auth0id_lt?: String
  auth0id_lte?: String
  auth0id_gt?: String
  auth0id_gte?: String
  auth0id_contains?: String
  auth0id_not_contains?: String
  auth0id_starts_with?: String
  auth0id_not_starts_with?: String
  auth0id_ends_with?: String
  auth0id_not_ends_with?: String
  username?: String
  username_not?: String
  username_in?: String[] | String
  username_not_in?: String[] | String
  username_lt?: String
  username_lte?: String
  username_gt?: String
  username_gte?: String
  username_contains?: String
  username_not_contains?: String
  username_starts_with?: String
  username_not_starts_with?: String
  username_ends_with?: String
  username_not_ends_with?: String
  avatar?: String
  avatar_not?: String
  avatar_in?: String[] | String
  avatar_not_in?: String[] | String
  avatar_lt?: String
  avatar_lte?: String
  avatar_gt?: String
  avatar_gte?: String
  avatar_contains?: String
  avatar_not_contains?: String
  avatar_starts_with?: String
  avatar_not_starts_with?: String
  avatar_ends_with?: String
  avatar_not_ends_with?: String
  tournaments_every?: TournamentMemberWhereInput
  tournaments_some?: TournamentMemberWhereInput
  tournaments_none?: TournamentMemberWhereInput
}

export interface FileWhereInput {
  AND?: FileWhereInput[] | FileWhereInput
  OR?: FileWhereInput[] | FileWhereInput
  NOT?: FileWhereInput[] | FileWhereInput
  contentType?: String
  contentType_not?: String
  contentType_in?: String[] | String
  contentType_not_in?: String[] | String
  contentType_lt?: String
  contentType_lte?: String
  contentType_gt?: String
  contentType_gte?: String
  contentType_contains?: String
  contentType_not_contains?: String
  contentType_starts_with?: String
  contentType_not_starts_with?: String
  contentType_ends_with?: String
  contentType_not_ends_with?: String
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  name?: String
  name_not?: String
  name_in?: String[] | String
  name_not_in?: String[] | String
  name_lt?: String
  name_lte?: String
  name_gt?: String
  name_gte?: String
  name_contains?: String
  name_not_contains?: String
  name_starts_with?: String
  name_not_starts_with?: String
  name_ends_with?: String
  name_not_ends_with?: String
  secret?: String
  secret_not?: String
  secret_in?: String[] | String
  secret_not_in?: String[] | String
  secret_lt?: String
  secret_lte?: String
  secret_gt?: String
  secret_gte?: String
  secret_contains?: String
  secret_not_contains?: String
  secret_starts_with?: String
  secret_not_starts_with?: String
  secret_ends_with?: String
  secret_not_ends_with?: String
  size?: Int
  size_not?: Int
  size_in?: Int[] | Int
  size_not_in?: Int[] | Int
  size_lt?: Int
  size_lte?: Int
  size_gt?: Int
  size_gte?: Int
  updatedAt?: DateTime
  updatedAt_not?: DateTime
  updatedAt_in?: DateTime[] | DateTime
  updatedAt_not_in?: DateTime[] | DateTime
  updatedAt_lt?: DateTime
  updatedAt_lte?: DateTime
  updatedAt_gt?: DateTime
  updatedAt_gte?: DateTime
  url?: String
  url_not?: String
  url_in?: String[] | String
  url_not_in?: String[] | String
  url_lt?: String
  url_lte?: String
  url_gt?: String
  url_gte?: String
  url_contains?: String
  url_not_contains?: String
  url_starts_with?: String
  url_not_starts_with?: String
  url_ends_with?: String
  url_not_ends_with?: String
}

export interface TournamentMemberWhereUniqueInput {
  id?: ID_Input
}

export interface BggInfoWhereUniqueInput {
  bggid?: Int
  id?: ID_Input
}

export interface FileWhereUniqueInput {
  id?: ID_Input
  secret?: String
  url?: String
}

export interface GameresultWhereUniqueInput {
  id?: ID_Input
}

export interface PlayerWhereUniqueInput {
  id?: ID_Input
}

export interface ScoreWhereUniqueInput {
  id?: ID_Input
}

export interface TournamentWhereUniqueInput {
  slug?: String
}

export interface InvitationWhereUniqueInput {
  token?: String
}

export interface UserWhereUniqueInput {
  auth0id?: String
}

export interface TournamentMemberCreateInput {
  role?: TournamentMemberRole
  tournament: TournamentCreateOneWithoutMembersInput
  user: UserCreateOneWithoutTournamentsInput
}

export interface TournamentCreateOneWithoutMembersInput {
  create?: TournamentCreateWithoutMembersInput
  connect?: TournamentWhereUniqueInput
}

export interface TournamentCreateWithoutMembersInput {
  slug: String
  name: String
  gameresults?: GameresultCreateManyWithoutTournamentInput
  players?: PlayerCreateManyWithoutTournamentInput
  invitations?: InvitationCreateManyWithoutTournamentInput
}

export interface GameresultCreateManyWithoutTournamentInput {
  create?:
    | GameresultCreateWithoutTournamentInput[]
    | GameresultCreateWithoutTournamentInput
  connect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput
}

export interface GameresultCreateWithoutTournamentInput {
  numPlayers?: Int
  time: DateTime
  bggInfo?: BggInfoCreateOneWithoutGameresultsInput
  scores?: ScoreCreateManyWithoutGameresultInput
}

export interface BggInfoCreateOneWithoutGameresultsInput {
  create?: BggInfoCreateWithoutGameresultsInput
  connect?: BggInfoWhereUniqueInput
}

export interface BggInfoCreateWithoutGameresultsInput {
  bggid: Int
  description: String
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  weight: Float
  yearPublished: Int
  lastReload?: DateTime
}

export interface ScoreCreateManyWithoutGameresultInput {
  create?:
    | ScoreCreateWithoutGameresultInput[]
    | ScoreCreateWithoutGameresultInput
  connect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput
}

export interface ScoreCreateWithoutGameresultInput {
  score: Int
  player: PlayerCreateOneWithoutScoresInput
}

export interface PlayerCreateOneWithoutScoresInput {
  create?: PlayerCreateWithoutScoresInput
  connect?: PlayerWhereUniqueInput
}

export interface PlayerCreateWithoutScoresInput {
  name: String
  tournament: TournamentCreateOneWithoutPlayersInput
}

export interface TournamentCreateOneWithoutPlayersInput {
  create?: TournamentCreateWithoutPlayersInput
  connect?: TournamentWhereUniqueInput
}

export interface TournamentCreateWithoutPlayersInput {
  slug: String
  name: String
  gameresults?: GameresultCreateManyWithoutTournamentInput
  members?: TournamentMemberCreateManyWithoutTournamentInput
  invitations?: InvitationCreateManyWithoutTournamentInput
}

export interface TournamentMemberCreateManyWithoutTournamentInput {
  create?:
    | TournamentMemberCreateWithoutTournamentInput[]
    | TournamentMemberCreateWithoutTournamentInput
  connect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
}

export interface TournamentMemberCreateWithoutTournamentInput {
  role?: TournamentMemberRole
  user: UserCreateOneWithoutTournamentsInput
}

export interface UserCreateOneWithoutTournamentsInput {
  create?: UserCreateWithoutTournamentsInput
  connect?: UserWhereUniqueInput
}

export interface UserCreateWithoutTournamentsInput {
  email: String
  auth0id: String
  username: String
  avatar: String
}

export interface InvitationCreateManyWithoutTournamentInput {
  create?:
    | InvitationCreateWithoutTournamentInput[]
    | InvitationCreateWithoutTournamentInput
  connect?: InvitationWhereUniqueInput[] | InvitationWhereUniqueInput
}

export interface InvitationCreateWithoutTournamentInput {
  token: String
  state?: InvitationState
  email: String
  inviter: UserCreateOneInput
}

export interface UserCreateOneInput {
  create?: UserCreateInput
  connect?: UserWhereUniqueInput
}

export interface UserCreateInput {
  email: String
  auth0id: String
  username: String
  avatar: String
  tournaments?: TournamentMemberCreateManyWithoutUserInput
}

export interface TournamentMemberCreateManyWithoutUserInput {
  create?:
    | TournamentMemberCreateWithoutUserInput[]
    | TournamentMemberCreateWithoutUserInput
  connect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
}

export interface TournamentMemberCreateWithoutUserInput {
  role?: TournamentMemberRole
  tournament: TournamentCreateOneWithoutMembersInput
}

export interface PlayerCreateManyWithoutTournamentInput {
  create?:
    | PlayerCreateWithoutTournamentInput[]
    | PlayerCreateWithoutTournamentInput
  connect?: PlayerWhereUniqueInput[] | PlayerWhereUniqueInput
}

export interface PlayerCreateWithoutTournamentInput {
  name: String
  scores?: ScoreCreateManyWithoutPlayerInput
}

export interface ScoreCreateManyWithoutPlayerInput {
  create?: ScoreCreateWithoutPlayerInput[] | ScoreCreateWithoutPlayerInput
  connect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput
}

export interface ScoreCreateWithoutPlayerInput {
  score: Int
  gameresult?: GameresultCreateOneWithoutScoresInput
}

export interface GameresultCreateOneWithoutScoresInput {
  create?: GameresultCreateWithoutScoresInput
  connect?: GameresultWhereUniqueInput
}

export interface GameresultCreateWithoutScoresInput {
  numPlayers?: Int
  time: DateTime
  bggInfo?: BggInfoCreateOneWithoutGameresultsInput
  tournament: TournamentCreateOneWithoutGameresultsInput
}

export interface TournamentCreateOneWithoutGameresultsInput {
  create?: TournamentCreateWithoutGameresultsInput
  connect?: TournamentWhereUniqueInput
}

export interface TournamentCreateWithoutGameresultsInput {
  slug: String
  name: String
  players?: PlayerCreateManyWithoutTournamentInput
  members?: TournamentMemberCreateManyWithoutTournamentInput
  invitations?: InvitationCreateManyWithoutTournamentInput
}

export interface BggInfoCreateInput {
  bggid: Int
  description: String
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  weight: Float
  yearPublished: Int
  lastReload?: DateTime
  gameresults?: GameresultCreateManyWithoutBggInfoInput
}

export interface GameresultCreateManyWithoutBggInfoInput {
  create?:
    | GameresultCreateWithoutBggInfoInput[]
    | GameresultCreateWithoutBggInfoInput
  connect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput
}

export interface GameresultCreateWithoutBggInfoInput {
  numPlayers?: Int
  time: DateTime
  scores?: ScoreCreateManyWithoutGameresultInput
  tournament: TournamentCreateOneWithoutGameresultsInput
}

export interface FileCreateInput {
  contentType: String
  name: String
  secret: String
  size: Int
  url: String
}

export interface GameresultCreateInput {
  numPlayers?: Int
  time: DateTime
  bggInfo?: BggInfoCreateOneWithoutGameresultsInput
  scores?: ScoreCreateManyWithoutGameresultInput
  tournament: TournamentCreateOneWithoutGameresultsInput
}

export interface PlayerCreateInput {
  name: String
  scores?: ScoreCreateManyWithoutPlayerInput
  tournament: TournamentCreateOneWithoutPlayersInput
}

export interface ScoreCreateInput {
  score: Int
  gameresult?: GameresultCreateOneWithoutScoresInput
  player: PlayerCreateOneWithoutScoresInput
}

export interface TournamentCreateInput {
  slug: String
  name: String
  gameresults?: GameresultCreateManyWithoutTournamentInput
  players?: PlayerCreateManyWithoutTournamentInput
  members?: TournamentMemberCreateManyWithoutTournamentInput
  invitations?: InvitationCreateManyWithoutTournamentInput
}

export interface InvitationCreateInput {
  token: String
  state?: InvitationState
  email: String
  inviter: UserCreateOneInput
  tournament: TournamentCreateOneWithoutInvitationsInput
}

export interface TournamentCreateOneWithoutInvitationsInput {
  create?: TournamentCreateWithoutInvitationsInput
  connect?: TournamentWhereUniqueInput
}

export interface TournamentCreateWithoutInvitationsInput {
  slug: String
  name: String
  gameresults?: GameresultCreateManyWithoutTournamentInput
  players?: PlayerCreateManyWithoutTournamentInput
  members?: TournamentMemberCreateManyWithoutTournamentInput
}

export interface TournamentMemberUpdateInput {
  role?: TournamentMemberRole
  tournament?: TournamentUpdateOneRequiredWithoutMembersInput
  user?: UserUpdateOneRequiredWithoutTournamentsInput
}

export interface TournamentUpdateOneRequiredWithoutMembersInput {
  create?: TournamentCreateWithoutMembersInput
  connect?: TournamentWhereUniqueInput
  update?: TournamentUpdateWithoutMembersDataInput
  upsert?: TournamentUpsertWithoutMembersInput
}

export interface TournamentUpdateWithoutMembersDataInput {
  slug?: String
  name?: String
  gameresults?: GameresultUpdateManyWithoutTournamentInput
  players?: PlayerUpdateManyWithoutTournamentInput
  invitations?: InvitationUpdateManyWithoutTournamentInput
}

export interface GameresultUpdateManyWithoutTournamentInput {
  create?:
    | GameresultCreateWithoutTournamentInput[]
    | GameresultCreateWithoutTournamentInput
  connect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput
  disconnect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput
  delete?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput
  update?:
    | GameresultUpdateWithWhereUniqueWithoutTournamentInput[]
    | GameresultUpdateWithWhereUniqueWithoutTournamentInput
  updateMany?:
    | GameresultUpdateManyWithWhereNestedInput[]
    | GameresultUpdateManyWithWhereNestedInput
  deleteMany?: GameresultScalarWhereInput[] | GameresultScalarWhereInput
  upsert?:
    | GameresultUpsertWithWhereUniqueWithoutTournamentInput[]
    | GameresultUpsertWithWhereUniqueWithoutTournamentInput
}

export interface GameresultUpdateWithWhereUniqueWithoutTournamentInput {
  where: GameresultWhereUniqueInput
  data: GameresultUpdateWithoutTournamentDataInput
}

export interface GameresultUpdateWithoutTournamentDataInput {
  numPlayers?: Int
  time?: DateTime
  bggInfo?: BggInfoUpdateOneWithoutGameresultsInput
  scores?: ScoreUpdateManyWithoutGameresultInput
}

export interface BggInfoUpdateOneWithoutGameresultsInput {
  create?: BggInfoCreateWithoutGameresultsInput
  connect?: BggInfoWhereUniqueInput
  disconnect?: Boolean
  delete?: Boolean
  update?: BggInfoUpdateWithoutGameresultsDataInput
  upsert?: BggInfoUpsertWithoutGameresultsInput
}

export interface BggInfoUpdateWithoutGameresultsDataInput {
  bggid?: Int
  description?: String
  maxPlayers?: Int
  name?: String
  ownedBy?: Int
  playTime?: Int
  rank?: Int
  rating?: Float
  thumbnail?: String
  weight?: Float
  yearPublished?: Int
  lastReload?: DateTime
}

export interface BggInfoUpsertWithoutGameresultsInput {
  update: BggInfoUpdateWithoutGameresultsDataInput
  create: BggInfoCreateWithoutGameresultsInput
}

export interface ScoreUpdateManyWithoutGameresultInput {
  create?:
    | ScoreCreateWithoutGameresultInput[]
    | ScoreCreateWithoutGameresultInput
  connect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput
  disconnect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput
  delete?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput
  update?:
    | ScoreUpdateWithWhereUniqueWithoutGameresultInput[]
    | ScoreUpdateWithWhereUniqueWithoutGameresultInput
  updateMany?:
    | ScoreUpdateManyWithWhereNestedInput[]
    | ScoreUpdateManyWithWhereNestedInput
  deleteMany?: ScoreScalarWhereInput[] | ScoreScalarWhereInput
  upsert?:
    | ScoreUpsertWithWhereUniqueWithoutGameresultInput[]
    | ScoreUpsertWithWhereUniqueWithoutGameresultInput
}

export interface ScoreUpdateWithWhereUniqueWithoutGameresultInput {
  where: ScoreWhereUniqueInput
  data: ScoreUpdateWithoutGameresultDataInput
}

export interface ScoreUpdateWithoutGameresultDataInput {
  score?: Int
  player?: PlayerUpdateOneRequiredWithoutScoresInput
}

export interface PlayerUpdateOneRequiredWithoutScoresInput {
  create?: PlayerCreateWithoutScoresInput
  connect?: PlayerWhereUniqueInput
  update?: PlayerUpdateWithoutScoresDataInput
  upsert?: PlayerUpsertWithoutScoresInput
}

export interface PlayerUpdateWithoutScoresDataInput {
  name?: String
  tournament?: TournamentUpdateOneRequiredWithoutPlayersInput
}

export interface TournamentUpdateOneRequiredWithoutPlayersInput {
  create?: TournamentCreateWithoutPlayersInput
  connect?: TournamentWhereUniqueInput
  update?: TournamentUpdateWithoutPlayersDataInput
  upsert?: TournamentUpsertWithoutPlayersInput
}

export interface TournamentUpdateWithoutPlayersDataInput {
  slug?: String
  name?: String
  gameresults?: GameresultUpdateManyWithoutTournamentInput
  members?: TournamentMemberUpdateManyWithoutTournamentInput
  invitations?: InvitationUpdateManyWithoutTournamentInput
}

export interface TournamentMemberUpdateManyWithoutTournamentInput {
  create?:
    | TournamentMemberCreateWithoutTournamentInput[]
    | TournamentMemberCreateWithoutTournamentInput
  connect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
  disconnect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
  delete?: TournamentMemberWhereUniqueInput[] | TournamentMemberWhereUniqueInput
  update?:
    | TournamentMemberUpdateWithWhereUniqueWithoutTournamentInput[]
    | TournamentMemberUpdateWithWhereUniqueWithoutTournamentInput
  updateMany?:
    | TournamentMemberUpdateManyWithWhereNestedInput[]
    | TournamentMemberUpdateManyWithWhereNestedInput
  deleteMany?:
    | TournamentMemberScalarWhereInput[]
    | TournamentMemberScalarWhereInput
  upsert?:
    | TournamentMemberUpsertWithWhereUniqueWithoutTournamentInput[]
    | TournamentMemberUpsertWithWhereUniqueWithoutTournamentInput
}

export interface TournamentMemberUpdateWithWhereUniqueWithoutTournamentInput {
  where: TournamentMemberWhereUniqueInput
  data: TournamentMemberUpdateWithoutTournamentDataInput
}

export interface TournamentMemberUpdateWithoutTournamentDataInput {
  role?: TournamentMemberRole
  user?: UserUpdateOneRequiredWithoutTournamentsInput
}

export interface UserUpdateOneRequiredWithoutTournamentsInput {
  create?: UserCreateWithoutTournamentsInput
  connect?: UserWhereUniqueInput
  update?: UserUpdateWithoutTournamentsDataInput
  upsert?: UserUpsertWithoutTournamentsInput
}

export interface UserUpdateWithoutTournamentsDataInput {
  email?: String
  auth0id?: String
  username?: String
  avatar?: String
}

export interface UserUpsertWithoutTournamentsInput {
  update: UserUpdateWithoutTournamentsDataInput
  create: UserCreateWithoutTournamentsInput
}

export interface TournamentMemberUpdateManyWithWhereNestedInput {
  where: TournamentMemberScalarWhereInput
  data: TournamentMemberUpdateManyDataInput
}

export interface TournamentMemberScalarWhereInput {
  AND?: TournamentMemberScalarWhereInput[] | TournamentMemberScalarWhereInput
  OR?: TournamentMemberScalarWhereInput[] | TournamentMemberScalarWhereInput
  NOT?: TournamentMemberScalarWhereInput[] | TournamentMemberScalarWhereInput
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  role?: TournamentMemberRole
  role_not?: TournamentMemberRole
  role_in?: TournamentMemberRole[] | TournamentMemberRole
  role_not_in?: TournamentMemberRole[] | TournamentMemberRole
}

export interface TournamentMemberUpdateManyDataInput {
  role?: TournamentMemberRole
}

export interface TournamentMemberUpsertWithWhereUniqueWithoutTournamentInput {
  where: TournamentMemberWhereUniqueInput
  update: TournamentMemberUpdateWithoutTournamentDataInput
  create: TournamentMemberCreateWithoutTournamentInput
}

export interface InvitationUpdateManyWithoutTournamentInput {
  create?:
    | InvitationCreateWithoutTournamentInput[]
    | InvitationCreateWithoutTournamentInput
  connect?: InvitationWhereUniqueInput[] | InvitationWhereUniqueInput
  disconnect?: InvitationWhereUniqueInput[] | InvitationWhereUniqueInput
  delete?: InvitationWhereUniqueInput[] | InvitationWhereUniqueInput
  update?:
    | InvitationUpdateWithWhereUniqueWithoutTournamentInput[]
    | InvitationUpdateWithWhereUniqueWithoutTournamentInput
  updateMany?:
    | InvitationUpdateManyWithWhereNestedInput[]
    | InvitationUpdateManyWithWhereNestedInput
  deleteMany?: InvitationScalarWhereInput[] | InvitationScalarWhereInput
  upsert?:
    | InvitationUpsertWithWhereUniqueWithoutTournamentInput[]
    | InvitationUpsertWithWhereUniqueWithoutTournamentInput
}

export interface InvitationUpdateWithWhereUniqueWithoutTournamentInput {
  where: InvitationWhereUniqueInput
  data: InvitationUpdateWithoutTournamentDataInput
}

export interface InvitationUpdateWithoutTournamentDataInput {
  token?: String
  state?: InvitationState
  email?: String
  inviter?: UserUpdateOneRequiredInput
}

export interface UserUpdateOneRequiredInput {
  create?: UserCreateInput
  connect?: UserWhereUniqueInput
  update?: UserUpdateDataInput
  upsert?: UserUpsertNestedInput
}

export interface UserUpdateDataInput {
  email?: String
  auth0id?: String
  username?: String
  avatar?: String
  tournaments?: TournamentMemberUpdateManyWithoutUserInput
}

export interface TournamentMemberUpdateManyWithoutUserInput {
  create?:
    | TournamentMemberCreateWithoutUserInput[]
    | TournamentMemberCreateWithoutUserInput
  connect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
  disconnect?:
    | TournamentMemberWhereUniqueInput[]
    | TournamentMemberWhereUniqueInput
  delete?: TournamentMemberWhereUniqueInput[] | TournamentMemberWhereUniqueInput
  update?:
    | TournamentMemberUpdateWithWhereUniqueWithoutUserInput[]
    | TournamentMemberUpdateWithWhereUniqueWithoutUserInput
  updateMany?:
    | TournamentMemberUpdateManyWithWhereNestedInput[]
    | TournamentMemberUpdateManyWithWhereNestedInput
  deleteMany?:
    | TournamentMemberScalarWhereInput[]
    | TournamentMemberScalarWhereInput
  upsert?:
    | TournamentMemberUpsertWithWhereUniqueWithoutUserInput[]
    | TournamentMemberUpsertWithWhereUniqueWithoutUserInput
}

export interface TournamentMemberUpdateWithWhereUniqueWithoutUserInput {
  where: TournamentMemberWhereUniqueInput
  data: TournamentMemberUpdateWithoutUserDataInput
}

export interface TournamentMemberUpdateWithoutUserDataInput {
  role?: TournamentMemberRole
  tournament?: TournamentUpdateOneRequiredWithoutMembersInput
}

export interface TournamentMemberUpsertWithWhereUniqueWithoutUserInput {
  where: TournamentMemberWhereUniqueInput
  update: TournamentMemberUpdateWithoutUserDataInput
  create: TournamentMemberCreateWithoutUserInput
}

export interface UserUpsertNestedInput {
  update: UserUpdateDataInput
  create: UserCreateInput
}

export interface InvitationUpdateManyWithWhereNestedInput {
  where: InvitationScalarWhereInput
  data: InvitationUpdateManyDataInput
}

export interface InvitationScalarWhereInput {
  AND?: InvitationScalarWhereInput[] | InvitationScalarWhereInput
  OR?: InvitationScalarWhereInput[] | InvitationScalarWhereInput
  NOT?: InvitationScalarWhereInput[] | InvitationScalarWhereInput
  token?: String
  token_not?: String
  token_in?: String[] | String
  token_not_in?: String[] | String
  token_lt?: String
  token_lte?: String
  token_gt?: String
  token_gte?: String
  token_contains?: String
  token_not_contains?: String
  token_starts_with?: String
  token_not_starts_with?: String
  token_ends_with?: String
  token_not_ends_with?: String
  state?: InvitationState
  state_not?: InvitationState
  state_in?: InvitationState[] | InvitationState
  state_not_in?: InvitationState[] | InvitationState
  email?: String
  email_not?: String
  email_in?: String[] | String
  email_not_in?: String[] | String
  email_lt?: String
  email_lte?: String
  email_gt?: String
  email_gte?: String
  email_contains?: String
  email_not_contains?: String
  email_starts_with?: String
  email_not_starts_with?: String
  email_ends_with?: String
  email_not_ends_with?: String
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  updatedAt?: DateTime
  updatedAt_not?: DateTime
  updatedAt_in?: DateTime[] | DateTime
  updatedAt_not_in?: DateTime[] | DateTime
  updatedAt_lt?: DateTime
  updatedAt_lte?: DateTime
  updatedAt_gt?: DateTime
  updatedAt_gte?: DateTime
}

export interface InvitationUpdateManyDataInput {
  token?: String
  state?: InvitationState
  email?: String
}

export interface InvitationUpsertWithWhereUniqueWithoutTournamentInput {
  where: InvitationWhereUniqueInput
  update: InvitationUpdateWithoutTournamentDataInput
  create: InvitationCreateWithoutTournamentInput
}

export interface TournamentUpsertWithoutPlayersInput {
  update: TournamentUpdateWithoutPlayersDataInput
  create: TournamentCreateWithoutPlayersInput
}

export interface PlayerUpsertWithoutScoresInput {
  update: PlayerUpdateWithoutScoresDataInput
  create: PlayerCreateWithoutScoresInput
}

export interface ScoreUpdateManyWithWhereNestedInput {
  where: ScoreScalarWhereInput
  data: ScoreUpdateManyDataInput
}

export interface ScoreScalarWhereInput {
  AND?: ScoreScalarWhereInput[] | ScoreScalarWhereInput
  OR?: ScoreScalarWhereInput[] | ScoreScalarWhereInput
  NOT?: ScoreScalarWhereInput[] | ScoreScalarWhereInput
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  score?: Int
  score_not?: Int
  score_in?: Int[] | Int
  score_not_in?: Int[] | Int
  score_lt?: Int
  score_lte?: Int
  score_gt?: Int
  score_gte?: Int
  updatedAt?: DateTime
  updatedAt_not?: DateTime
  updatedAt_in?: DateTime[] | DateTime
  updatedAt_not_in?: DateTime[] | DateTime
  updatedAt_lt?: DateTime
  updatedAt_lte?: DateTime
  updatedAt_gt?: DateTime
  updatedAt_gte?: DateTime
}

export interface ScoreUpdateManyDataInput {
  score?: Int
}

export interface ScoreUpsertWithWhereUniqueWithoutGameresultInput {
  where: ScoreWhereUniqueInput
  update: ScoreUpdateWithoutGameresultDataInput
  create: ScoreCreateWithoutGameresultInput
}

export interface GameresultUpdateManyWithWhereNestedInput {
  where: GameresultScalarWhereInput
  data: GameresultUpdateManyDataInput
}

export interface GameresultScalarWhereInput {
  AND?: GameresultScalarWhereInput[] | GameresultScalarWhereInput
  OR?: GameresultScalarWhereInput[] | GameresultScalarWhereInput
  NOT?: GameresultScalarWhereInput[] | GameresultScalarWhereInput
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  numPlayers?: Int
  numPlayers_not?: Int
  numPlayers_in?: Int[] | Int
  numPlayers_not_in?: Int[] | Int
  numPlayers_lt?: Int
  numPlayers_lte?: Int
  numPlayers_gt?: Int
  numPlayers_gte?: Int
  time?: DateTime
  time_not?: DateTime
  time_in?: DateTime[] | DateTime
  time_not_in?: DateTime[] | DateTime
  time_lt?: DateTime
  time_lte?: DateTime
  time_gt?: DateTime
  time_gte?: DateTime
  updatedAt?: DateTime
  updatedAt_not?: DateTime
  updatedAt_in?: DateTime[] | DateTime
  updatedAt_not_in?: DateTime[] | DateTime
  updatedAt_lt?: DateTime
  updatedAt_lte?: DateTime
  updatedAt_gt?: DateTime
  updatedAt_gte?: DateTime
}

export interface GameresultUpdateManyDataInput {
  numPlayers?: Int
  time?: DateTime
}

export interface GameresultUpsertWithWhereUniqueWithoutTournamentInput {
  where: GameresultWhereUniqueInput
  update: GameresultUpdateWithoutTournamentDataInput
  create: GameresultCreateWithoutTournamentInput
}

export interface PlayerUpdateManyWithoutTournamentInput {
  create?:
    | PlayerCreateWithoutTournamentInput[]
    | PlayerCreateWithoutTournamentInput
  connect?: PlayerWhereUniqueInput[] | PlayerWhereUniqueInput
  disconnect?: PlayerWhereUniqueInput[] | PlayerWhereUniqueInput
  delete?: PlayerWhereUniqueInput[] | PlayerWhereUniqueInput
  update?:
    | PlayerUpdateWithWhereUniqueWithoutTournamentInput[]
    | PlayerUpdateWithWhereUniqueWithoutTournamentInput
  updateMany?:
    | PlayerUpdateManyWithWhereNestedInput[]
    | PlayerUpdateManyWithWhereNestedInput
  deleteMany?: PlayerScalarWhereInput[] | PlayerScalarWhereInput
  upsert?:
    | PlayerUpsertWithWhereUniqueWithoutTournamentInput[]
    | PlayerUpsertWithWhereUniqueWithoutTournamentInput
}

export interface PlayerUpdateWithWhereUniqueWithoutTournamentInput {
  where: PlayerWhereUniqueInput
  data: PlayerUpdateWithoutTournamentDataInput
}

export interface PlayerUpdateWithoutTournamentDataInput {
  name?: String
  scores?: ScoreUpdateManyWithoutPlayerInput
}

export interface ScoreUpdateManyWithoutPlayerInput {
  create?: ScoreCreateWithoutPlayerInput[] | ScoreCreateWithoutPlayerInput
  connect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput
  disconnect?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput
  delete?: ScoreWhereUniqueInput[] | ScoreWhereUniqueInput
  update?:
    | ScoreUpdateWithWhereUniqueWithoutPlayerInput[]
    | ScoreUpdateWithWhereUniqueWithoutPlayerInput
  updateMany?:
    | ScoreUpdateManyWithWhereNestedInput[]
    | ScoreUpdateManyWithWhereNestedInput
  deleteMany?: ScoreScalarWhereInput[] | ScoreScalarWhereInput
  upsert?:
    | ScoreUpsertWithWhereUniqueWithoutPlayerInput[]
    | ScoreUpsertWithWhereUniqueWithoutPlayerInput
}

export interface ScoreUpdateWithWhereUniqueWithoutPlayerInput {
  where: ScoreWhereUniqueInput
  data: ScoreUpdateWithoutPlayerDataInput
}

export interface ScoreUpdateWithoutPlayerDataInput {
  score?: Int
  gameresult?: GameresultUpdateOneWithoutScoresInput
}

export interface GameresultUpdateOneWithoutScoresInput {
  create?: GameresultCreateWithoutScoresInput
  connect?: GameresultWhereUniqueInput
  disconnect?: Boolean
  delete?: Boolean
  update?: GameresultUpdateWithoutScoresDataInput
  upsert?: GameresultUpsertWithoutScoresInput
}

export interface GameresultUpdateWithoutScoresDataInput {
  numPlayers?: Int
  time?: DateTime
  bggInfo?: BggInfoUpdateOneWithoutGameresultsInput
  tournament?: TournamentUpdateOneRequiredWithoutGameresultsInput
}

export interface TournamentUpdateOneRequiredWithoutGameresultsInput {
  create?: TournamentCreateWithoutGameresultsInput
  connect?: TournamentWhereUniqueInput
  update?: TournamentUpdateWithoutGameresultsDataInput
  upsert?: TournamentUpsertWithoutGameresultsInput
}

export interface TournamentUpdateWithoutGameresultsDataInput {
  slug?: String
  name?: String
  players?: PlayerUpdateManyWithoutTournamentInput
  members?: TournamentMemberUpdateManyWithoutTournamentInput
  invitations?: InvitationUpdateManyWithoutTournamentInput
}

export interface TournamentUpsertWithoutGameresultsInput {
  update: TournamentUpdateWithoutGameresultsDataInput
  create: TournamentCreateWithoutGameresultsInput
}

export interface GameresultUpsertWithoutScoresInput {
  update: GameresultUpdateWithoutScoresDataInput
  create: GameresultCreateWithoutScoresInput
}

export interface ScoreUpsertWithWhereUniqueWithoutPlayerInput {
  where: ScoreWhereUniqueInput
  update: ScoreUpdateWithoutPlayerDataInput
  create: ScoreCreateWithoutPlayerInput
}

export interface PlayerUpdateManyWithWhereNestedInput {
  where: PlayerScalarWhereInput
  data: PlayerUpdateManyDataInput
}

export interface PlayerScalarWhereInput {
  AND?: PlayerScalarWhereInput[] | PlayerScalarWhereInput
  OR?: PlayerScalarWhereInput[] | PlayerScalarWhereInput
  NOT?: PlayerScalarWhereInput[] | PlayerScalarWhereInput
  createdAt?: DateTime
  createdAt_not?: DateTime
  createdAt_in?: DateTime[] | DateTime
  createdAt_not_in?: DateTime[] | DateTime
  createdAt_lt?: DateTime
  createdAt_lte?: DateTime
  createdAt_gt?: DateTime
  createdAt_gte?: DateTime
  id?: ID_Input
  id_not?: ID_Input
  id_in?: ID_Input[] | ID_Input
  id_not_in?: ID_Input[] | ID_Input
  id_lt?: ID_Input
  id_lte?: ID_Input
  id_gt?: ID_Input
  id_gte?: ID_Input
  id_contains?: ID_Input
  id_not_contains?: ID_Input
  id_starts_with?: ID_Input
  id_not_starts_with?: ID_Input
  id_ends_with?: ID_Input
  id_not_ends_with?: ID_Input
  name?: String
  name_not?: String
  name_in?: String[] | String
  name_not_in?: String[] | String
  name_lt?: String
  name_lte?: String
  name_gt?: String
  name_gte?: String
  name_contains?: String
  name_not_contains?: String
  name_starts_with?: String
  name_not_starts_with?: String
  name_ends_with?: String
  name_not_ends_with?: String
  updatedAt?: DateTime
  updatedAt_not?: DateTime
  updatedAt_in?: DateTime[] | DateTime
  updatedAt_not_in?: DateTime[] | DateTime
  updatedAt_lt?: DateTime
  updatedAt_lte?: DateTime
  updatedAt_gt?: DateTime
  updatedAt_gte?: DateTime
}

export interface PlayerUpdateManyDataInput {
  name?: String
}

export interface PlayerUpsertWithWhereUniqueWithoutTournamentInput {
  where: PlayerWhereUniqueInput
  update: PlayerUpdateWithoutTournamentDataInput
  create: PlayerCreateWithoutTournamentInput
}

export interface TournamentUpsertWithoutMembersInput {
  update: TournamentUpdateWithoutMembersDataInput
  create: TournamentCreateWithoutMembersInput
}

export interface BggInfoUpdateInput {
  bggid?: Int
  description?: String
  maxPlayers?: Int
  name?: String
  ownedBy?: Int
  playTime?: Int
  rank?: Int
  rating?: Float
  thumbnail?: String
  weight?: Float
  yearPublished?: Int
  lastReload?: DateTime
  gameresults?: GameresultUpdateManyWithoutBggInfoInput
}

export interface GameresultUpdateManyWithoutBggInfoInput {
  create?:
    | GameresultCreateWithoutBggInfoInput[]
    | GameresultCreateWithoutBggInfoInput
  connect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput
  disconnect?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput
  delete?: GameresultWhereUniqueInput[] | GameresultWhereUniqueInput
  update?:
    | GameresultUpdateWithWhereUniqueWithoutBggInfoInput[]
    | GameresultUpdateWithWhereUniqueWithoutBggInfoInput
  updateMany?:
    | GameresultUpdateManyWithWhereNestedInput[]
    | GameresultUpdateManyWithWhereNestedInput
  deleteMany?: GameresultScalarWhereInput[] | GameresultScalarWhereInput
  upsert?:
    | GameresultUpsertWithWhereUniqueWithoutBggInfoInput[]
    | GameresultUpsertWithWhereUniqueWithoutBggInfoInput
}

export interface GameresultUpdateWithWhereUniqueWithoutBggInfoInput {
  where: GameresultWhereUniqueInput
  data: GameresultUpdateWithoutBggInfoDataInput
}

export interface GameresultUpdateWithoutBggInfoDataInput {
  numPlayers?: Int
  time?: DateTime
  scores?: ScoreUpdateManyWithoutGameresultInput
  tournament?: TournamentUpdateOneRequiredWithoutGameresultsInput
}

export interface GameresultUpsertWithWhereUniqueWithoutBggInfoInput {
  where: GameresultWhereUniqueInput
  update: GameresultUpdateWithoutBggInfoDataInput
  create: GameresultCreateWithoutBggInfoInput
}

export interface FileUpdateInput {
  contentType?: String
  name?: String
  secret?: String
  size?: Int
  url?: String
}

export interface GameresultUpdateInput {
  numPlayers?: Int
  time?: DateTime
  bggInfo?: BggInfoUpdateOneWithoutGameresultsInput
  scores?: ScoreUpdateManyWithoutGameresultInput
  tournament?: TournamentUpdateOneRequiredWithoutGameresultsInput
}

export interface PlayerUpdateInput {
  name?: String
  scores?: ScoreUpdateManyWithoutPlayerInput
  tournament?: TournamentUpdateOneRequiredWithoutPlayersInput
}

export interface ScoreUpdateInput {
  score?: Int
  gameresult?: GameresultUpdateOneWithoutScoresInput
  player?: PlayerUpdateOneRequiredWithoutScoresInput
}

export interface TournamentUpdateInput {
  slug?: String
  name?: String
  gameresults?: GameresultUpdateManyWithoutTournamentInput
  players?: PlayerUpdateManyWithoutTournamentInput
  members?: TournamentMemberUpdateManyWithoutTournamentInput
  invitations?: InvitationUpdateManyWithoutTournamentInput
}

export interface InvitationUpdateInput {
  token?: String
  state?: InvitationState
  email?: String
  inviter?: UserUpdateOneRequiredInput
  tournament?: TournamentUpdateOneRequiredWithoutInvitationsInput
}

export interface TournamentUpdateOneRequiredWithoutInvitationsInput {
  create?: TournamentCreateWithoutInvitationsInput
  connect?: TournamentWhereUniqueInput
  update?: TournamentUpdateWithoutInvitationsDataInput
  upsert?: TournamentUpsertWithoutInvitationsInput
}

export interface TournamentUpdateWithoutInvitationsDataInput {
  slug?: String
  name?: String
  gameresults?: GameresultUpdateManyWithoutTournamentInput
  players?: PlayerUpdateManyWithoutTournamentInput
  members?: TournamentMemberUpdateManyWithoutTournamentInput
}

export interface TournamentUpsertWithoutInvitationsInput {
  update: TournamentUpdateWithoutInvitationsDataInput
  create: TournamentCreateWithoutInvitationsInput
}

export interface UserUpdateInput {
  email?: String
  auth0id?: String
  username?: String
  avatar?: String
  tournaments?: TournamentMemberUpdateManyWithoutUserInput
}

export interface TournamentMemberUpdateManyMutationInput {
  role?: TournamentMemberRole
}

export interface BggInfoUpdateManyMutationInput {
  bggid?: Int
  description?: String
  maxPlayers?: Int
  name?: String
  ownedBy?: Int
  playTime?: Int
  rank?: Int
  rating?: Float
  thumbnail?: String
  weight?: Float
  yearPublished?: Int
  lastReload?: DateTime
}

export interface FileUpdateManyMutationInput {
  contentType?: String
  name?: String
  secret?: String
  size?: Int
  url?: String
}

export interface GameresultUpdateManyMutationInput {
  numPlayers?: Int
  time?: DateTime
}

export interface PlayerUpdateManyMutationInput {
  name?: String
}

export interface ScoreUpdateManyMutationInput {
  score?: Int
}

export interface TournamentUpdateManyMutationInput {
  slug?: String
  name?: String
}

export interface InvitationUpdateManyMutationInput {
  token?: String
  state?: InvitationState
  email?: String
}

export interface UserUpdateManyMutationInput {
  email?: String
  auth0id?: String
  username?: String
  avatar?: String
}

export interface TournamentMemberSubscriptionWhereInput {
  AND?:
    | TournamentMemberSubscriptionWhereInput[]
    | TournamentMemberSubscriptionWhereInput
  OR?:
    | TournamentMemberSubscriptionWhereInput[]
    | TournamentMemberSubscriptionWhereInput
  NOT?:
    | TournamentMemberSubscriptionWhereInput[]
    | TournamentMemberSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: TournamentMemberWhereInput
}

export interface BggInfoSubscriptionWhereInput {
  AND?: BggInfoSubscriptionWhereInput[] | BggInfoSubscriptionWhereInput
  OR?: BggInfoSubscriptionWhereInput[] | BggInfoSubscriptionWhereInput
  NOT?: BggInfoSubscriptionWhereInput[] | BggInfoSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: BggInfoWhereInput
}

export interface FileSubscriptionWhereInput {
  AND?: FileSubscriptionWhereInput[] | FileSubscriptionWhereInput
  OR?: FileSubscriptionWhereInput[] | FileSubscriptionWhereInput
  NOT?: FileSubscriptionWhereInput[] | FileSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: FileWhereInput
}

export interface GameresultSubscriptionWhereInput {
  AND?: GameresultSubscriptionWhereInput[] | GameresultSubscriptionWhereInput
  OR?: GameresultSubscriptionWhereInput[] | GameresultSubscriptionWhereInput
  NOT?: GameresultSubscriptionWhereInput[] | GameresultSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: GameresultWhereInput
}

export interface PlayerSubscriptionWhereInput {
  AND?: PlayerSubscriptionWhereInput[] | PlayerSubscriptionWhereInput
  OR?: PlayerSubscriptionWhereInput[] | PlayerSubscriptionWhereInput
  NOT?: PlayerSubscriptionWhereInput[] | PlayerSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: PlayerWhereInput
}

export interface ScoreSubscriptionWhereInput {
  AND?: ScoreSubscriptionWhereInput[] | ScoreSubscriptionWhereInput
  OR?: ScoreSubscriptionWhereInput[] | ScoreSubscriptionWhereInput
  NOT?: ScoreSubscriptionWhereInput[] | ScoreSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: ScoreWhereInput
}

export interface TournamentSubscriptionWhereInput {
  AND?: TournamentSubscriptionWhereInput[] | TournamentSubscriptionWhereInput
  OR?: TournamentSubscriptionWhereInput[] | TournamentSubscriptionWhereInput
  NOT?: TournamentSubscriptionWhereInput[] | TournamentSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: TournamentWhereInput
}

export interface InvitationSubscriptionWhereInput {
  AND?: InvitationSubscriptionWhereInput[] | InvitationSubscriptionWhereInput
  OR?: InvitationSubscriptionWhereInput[] | InvitationSubscriptionWhereInput
  NOT?: InvitationSubscriptionWhereInput[] | InvitationSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: InvitationWhereInput
}

export interface UserSubscriptionWhereInput {
  AND?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput
  OR?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput
  NOT?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput
  mutation_in?: MutationType[] | MutationType
  updatedFields_contains?: String
  updatedFields_contains_every?: String[] | String
  updatedFields_contains_some?: String[] | String
  node?: UserWhereInput
}

/*
 * An object with an ID

 */
export interface Node {
  id: ID_Output
}

export interface TournamentMember extends Node {
  id: ID_Output
  tournament: Tournament
  user: User
  role: TournamentMemberRole
}

export interface Tournament {
  slug: String
  gameresults?: Gameresult[]
  name: String
  players?: Player[]
  members?: TournamentMember[]
  invitations?: Invitation[]
  createdAt: DateTime
  updatedAt: DateTime
}

export interface Gameresult extends Node {
  bggInfo?: BggInfo
  createdAt: DateTime
  id: ID_Output
  numPlayers?: Int
  scores?: Score[]
  time: DateTime
  tournament: Tournament
  updatedAt: DateTime
}

export interface BggInfo extends Node {
  bggid: Int
  createdAt: DateTime
  description: String
  gameresults?: Gameresult[]
  id: ID_Output
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  updatedAt: DateTime
  weight: Float
  yearPublished: Int
  lastReload: DateTime
}

export interface Score extends Node {
  createdAt: DateTime
  gameresult?: Gameresult
  id: ID_Output
  player: Player
  score: Int
  updatedAt: DateTime
}

export interface Player extends Node {
  createdAt: DateTime
  id: ID_Output
  name: String
  scores?: Score[]
  tournament: Tournament
  updatedAt: DateTime
}

export interface Invitation {
  token: String
  state: InvitationState
  email: String
  inviter: User
  tournament: Tournament
  createdAt: DateTime
  updatedAt: DateTime
}

export interface User {
  email: String
  auth0id: String
  username: String
  avatar: String
  tournaments?: TournamentMember[]
}

export interface File extends Node {
  contentType: String
  createdAt: DateTime
  id: ID_Output
  name: String
  secret: String
  size: Int
  updatedAt: DateTime
  url: String
}

/*
 * A connection to a list of items.

 */
export interface TournamentMemberConnection {
  pageInfo: PageInfo
  edges: TournamentMemberEdge[]
  aggregate: AggregateTournamentMember
}

/*
 * Information about pagination in a connection.

 */
export interface PageInfo {
  hasNextPage: Boolean
  hasPreviousPage: Boolean
  startCursor?: String
  endCursor?: String
}

/*
 * An edge in a connection.

 */
export interface TournamentMemberEdge {
  node: TournamentMember
  cursor: String
}

export interface AggregateTournamentMember {
  count: Int
}

/*
 * A connection to a list of items.

 */
export interface BggInfoConnection {
  pageInfo: PageInfo
  edges: BggInfoEdge[]
  aggregate: AggregateBggInfo
}

/*
 * An edge in a connection.

 */
export interface BggInfoEdge {
  node: BggInfo
  cursor: String
}

export interface AggregateBggInfo {
  count: Int
}

/*
 * A connection to a list of items.

 */
export interface FileConnection {
  pageInfo: PageInfo
  edges: FileEdge[]
  aggregate: AggregateFile
}

/*
 * An edge in a connection.

 */
export interface FileEdge {
  node: File
  cursor: String
}

export interface AggregateFile {
  count: Int
}

/*
 * A connection to a list of items.

 */
export interface GameresultConnection {
  pageInfo: PageInfo
  edges: GameresultEdge[]
  aggregate: AggregateGameresult
}

/*
 * An edge in a connection.

 */
export interface GameresultEdge {
  node: Gameresult
  cursor: String
}

export interface AggregateGameresult {
  count: Int
}

/*
 * A connection to a list of items.

 */
export interface PlayerConnection {
  pageInfo: PageInfo
  edges: PlayerEdge[]
  aggregate: AggregatePlayer
}

/*
 * An edge in a connection.

 */
export interface PlayerEdge {
  node: Player
  cursor: String
}

export interface AggregatePlayer {
  count: Int
}

/*
 * A connection to a list of items.

 */
export interface ScoreConnection {
  pageInfo: PageInfo
  edges: ScoreEdge[]
  aggregate: AggregateScore
}

/*
 * An edge in a connection.

 */
export interface ScoreEdge {
  node: Score
  cursor: String
}

export interface AggregateScore {
  count: Int
}

/*
 * A connection to a list of items.

 */
export interface TournamentConnection {
  pageInfo: PageInfo
  edges: TournamentEdge[]
  aggregate: AggregateTournament
}

/*
 * An edge in a connection.

 */
export interface TournamentEdge {
  node: Tournament
  cursor: String
}

export interface AggregateTournament {
  count: Int
}

/*
 * A connection to a list of items.

 */
export interface InvitationConnection {
  pageInfo: PageInfo
  edges: InvitationEdge[]
  aggregate: AggregateInvitation
}

/*
 * An edge in a connection.

 */
export interface InvitationEdge {
  node: Invitation
  cursor: String
}

export interface AggregateInvitation {
  count: Int
}

/*
 * A connection to a list of items.

 */
export interface UserConnection {
  pageInfo: PageInfo
  edges: UserEdge[]
  aggregate: AggregateUser
}

/*
 * An edge in a connection.

 */
export interface UserEdge {
  node: User
  cursor: String
}

export interface AggregateUser {
  count: Int
}

export interface BatchPayload {
  count: Long
}

export interface TournamentMemberSubscriptionPayload {
  mutation: MutationType
  node?: TournamentMember
  updatedFields?: String[]
  previousValues?: TournamentMemberPreviousValues
}

export interface TournamentMemberPreviousValues {
  id: ID_Output
  role: TournamentMemberRole
}

export interface BggInfoSubscriptionPayload {
  mutation: MutationType
  node?: BggInfo
  updatedFields?: String[]
  previousValues?: BggInfoPreviousValues
}

export interface BggInfoPreviousValues {
  bggid: Int
  createdAt: DateTime
  description: String
  id: ID_Output
  maxPlayers: Int
  name: String
  ownedBy: Int
  playTime: Int
  rank: Int
  rating: Float
  thumbnail: String
  updatedAt: DateTime
  weight: Float
  yearPublished: Int
  lastReload: DateTime
}

export interface FileSubscriptionPayload {
  mutation: MutationType
  node?: File
  updatedFields?: String[]
  previousValues?: FilePreviousValues
}

export interface FilePreviousValues {
  contentType: String
  createdAt: DateTime
  id: ID_Output
  name: String
  secret: String
  size: Int
  updatedAt: DateTime
  url: String
}

export interface GameresultSubscriptionPayload {
  mutation: MutationType
  node?: Gameresult
  updatedFields?: String[]
  previousValues?: GameresultPreviousValues
}

export interface GameresultPreviousValues {
  createdAt: DateTime
  id: ID_Output
  numPlayers?: Int
  time: DateTime
  updatedAt: DateTime
}

export interface PlayerSubscriptionPayload {
  mutation: MutationType
  node?: Player
  updatedFields?: String[]
  previousValues?: PlayerPreviousValues
}

export interface PlayerPreviousValues {
  createdAt: DateTime
  id: ID_Output
  name: String
  updatedAt: DateTime
}

export interface ScoreSubscriptionPayload {
  mutation: MutationType
  node?: Score
  updatedFields?: String[]
  previousValues?: ScorePreviousValues
}

export interface ScorePreviousValues {
  createdAt: DateTime
  id: ID_Output
  score: Int
  updatedAt: DateTime
}

export interface TournamentSubscriptionPayload {
  mutation: MutationType
  node?: Tournament
  updatedFields?: String[]
  previousValues?: TournamentPreviousValues
}

export interface TournamentPreviousValues {
  slug: String
  name: String
  createdAt: DateTime
  updatedAt: DateTime
}

export interface InvitationSubscriptionPayload {
  mutation: MutationType
  node?: Invitation
  updatedFields?: String[]
  previousValues?: InvitationPreviousValues
}

export interface InvitationPreviousValues {
  token: String
  state: InvitationState
  email: String
  createdAt: DateTime
  updatedAt: DateTime
}

export interface UserSubscriptionPayload {
  mutation: MutationType
  node?: User
  updatedFields?: String[]
  previousValues?: UserPreviousValues
}

export interface UserPreviousValues {
  email: String
  auth0id: String
  username: String
  avatar: String
}

/*
The `ID` scalar type represents a unique identifier, often used to refetch an object or as key for a cache. The ID type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"4"`) or integer (such as `4`) input value will be accepted as an ID.
*/
export type ID_Input = string | number
export type ID_Output = string

/*
The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text.
*/
export type String = string

export type DateTime = Date | string

/*
The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1. 
*/
export type Int = number

/*
The `Float` scalar type represents signed double-precision fractional values as specified by [IEEE 754](http://en.wikipedia.org/wiki/IEEE_floating_point). 
*/
export type Float = number

/*
The `Boolean` scalar type represents `true` or `false`.
*/
export type Boolean = boolean

/*
The `Long` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
*/
export type Long = string
