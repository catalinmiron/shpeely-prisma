const jwt = require('express-jwt')
const jwksRsa = require('jwks-rsa')

// export const checkJwt = jwt({
// 	// Dynamically provide a signing key
// 	// based on the kid in the header and
// 	// the signing keys provided by the JWKS endpoint.
// 	secret: process.env.AUTH0_CLIENT_SECRET,
//
// 	// Validate the audience and the issuer.
// 	credentialsRequired: false,
// 	audience: process.env.AUTH0_AUDIENCE,
// 	issuer: process.env.AUTH0_ISSUER,
// 	algorithms: [`RS256`]
// })

export const checkJwt = jwt({
  // Dynamically provide a signing key based on the kid in the header and the singing keys provided by the JWKS endpoint.
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://shpeely.auth0.com/.well-known/jwks.json`
  }),

  // Validate the audience and the issuer.
  // audience: 'urn:my-resource-server',
  issuer: 'https://shpeely.auth0.com/',
  algorithms: ['RS256']
})
