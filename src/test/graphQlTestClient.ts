import { agent } from 'supertest'
import * as getSlug from 'speakingurl'
import { tmpdir } from 'os'
import * as fs from 'fs'
import * as path from 'path'
import * as logger from 'winston'
import axios from 'axios'

class GraphQLError extends Error {
  public errors: any[]
}

interface Body {
  data?: any
  errors?: Error[]
}

export class GraphQlTestClient {
  private tokenFileName = 'shpeely-token.json'

  constructor(private app: any, private headers: any = {}) {}

  private tokenFilePath(username: string) {
    return path.join(tmpdir(), `${getSlug(username)}.${this.tokenFileName}`)
  }

  private loadToken(username) {
    try {
      const token = JSON.parse(
        fs.readFileSync(this.tokenFilePath(username)).toString()
      )
      const stats = fs.statSync(this.tokenFilePath(username))

      // check if token is still valid
      if (
        (new Date().getTime() - stats.mtime.getTime()) / 1000 -
          token.expires_in >=
        0
      ) {
        return null
      }

      logger.debug(`Got token from disk`)
      return token
    } catch {
      return null
    }
  }

  private saveToken(username: string, token: any) {
    const tokenPath = this.tokenFilePath(username)
    logger.debug(`Saving test token at ${tokenPath}`)
    return new Promise(resolve => {
      fs.writeFile(tokenPath, JSON.stringify(token), resolve)
    })
  }

  async unauthenticate() {
    logger.debug('Unauthenticate client')
    if (this.headers.Authorization) {
      delete this.headers.Authorization
    }
  }

  async authenticate(username: string, password: string) {
    logger.debug(`Authenticate user ${username}`)
    let token = this.loadToken(username)

    if (!token) {
      const { data } = await axios.post(
        `https://${process.env.AUTH0_DOMAIN}/oauth/token`,
        {
          username,
          password,
          grant_type: 'password',
          audience: `https://${process.env.AUTH0_DOMAIN}/userinfo`,
          scope: 'openid profile email',
          client_id: process.env.AUTH0_CLIENT_ID,
          client_secret: process.env.AUTH0_CLIENT_SECRET
        },
        {
          headers: {
            'content-type': 'application/json'
          }
        }
      )

      token = data

      logger.debug(`Got token from auth0: ${JSON.stringify(token)}`)
      await this.saveToken(username, token)
    }

    const idToken = token.id_token

    logger.debug(`id token from auth0 is ${idToken}`)

    this.headers = {
      Authorization: `Bearer ${idToken}`
    }

    logger.debug('Authenticating in prisma with the auth0 token...')

    const res = await this.request(
      `
			mutation ($idToken: String!) {
				authenticate(idToken: $idToken) {
					auth0id
				}
			}
		`,
      { idToken }
    )

    logger.debug('Prisma authentication complete')
  }

  async request(query: string, variables: any = {}, headers = {}) {
    const finalHeaders = { ...this.headers, ...headers }
    logger.debug(`Sending request with headers ${JSON.stringify(finalHeaders)}`)
    logger.debug(`Request query: ${query}`)
    const { body } = await agent(this.app)
      .post('/')
      .set(finalHeaders)
      .send({
        query,
        variables
      })

    if (body.errors) {
      logger.error('GraphQL request has errors')
      body.errors.forEach(e => console.error(e))

      const err = new GraphQLError()
      err.errors = body.errors

      throw err
    }

    expect(body).toHaveProperty('data')

    return body.data
  }

  close() {
    this.app.close()
  }
}
