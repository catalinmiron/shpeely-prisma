const { spawnSync } = require('child_process')
const { join } = require('path')

const spawnConfig = {
  cwd: join(__filename, '../../..'),
  env: {
    ...process.env,
    // if set to 'test' prisma will create a temp home dir and cannot connect to the test server in the cloud
    NODE_ENV: 'development'
  },
  stdio: [0, 1, 2]
}

async function prisma(...args) {
  return spawnSync('prisma', args, spawnConfig)
}

exports.prisma = prisma

exports.deployPrisma = async function() {
  return prisma('deploy')
}

exports.resetPrisma = async function() {
  return prisma('reset', '--force')
}
