import { GraphQlError } from '../errors'
import { GraphQlTestClient } from './graphQlTestClient'
import { TournamentMember, TournamentMemberRole } from '../generated/prisma'

export async function expectGraphQlError(
  fn: () => Promise<any>,
  error?: string | GraphQlError
): Promise<void> {
  let err

  try {
    await fn()
  } catch (e) {
    err = e
  }

  if (!err) {
    fail('Expected to throw an error')
  }

  if (error) {
    expect(err.errors.some(({ message }) => message.includes(error))).toBe(true)
  }
}

export async function createRandomTournament(
  client: GraphQlTestClient,
  overrideQuery?: string
): Promise<any> {
  const tournamentName = `Tournament ${Math.round(Math.random() * 10000)}`
  return createTournament(client, tournamentName, overrideQuery)
}

export async function createTournament(
  client: GraphQlTestClient,
  name: string,
  overrideQuery?: string
): Promise<any> {
  const query =
    overrideQuery ||
    `
		mutation ($name: String!) {
			createTournament(data: {name: $name}) {
				name
				slug
				members {
					role
				}
			}
		}
	`

  const variables = {
    name
  }

  const { createTournament: tournament } = await client.request(query, {
    name
  })

  return tournament
}

export async function createInvitation(
  client: GraphQlTestClient,
  slug: string,
  email: string = 'mail@example.com'
) {
  const query = `
		mutation ($email: String!, $slug: String!) {
			createInvitation(data: {email: $email, slug: $slug}) {
				state
				token
				tournament {
					name
					slug
				}
				inviter {
					auth0id
					avatar
					username
				}
			}
		}
	`

  const variables = {
    email,
    slug
  }

  const { createInvitation: invitation } = await client.request(
    query,
    variables
  )

  return invitation
}

export async function acceptInvitation(
  client: GraphQlTestClient,
  token: string
) {
  const query = `
		mutation ($token: String!) {
			acceptInvitation(where: {token: $token}) {
				state
				tournament {
					name
					slug
				}
				inviter {
					auth0id
					avatar
					username
				}
			}
		}
	`

  const variables = {
    token
  }

  return client.request(query, variables)
}

export async function createGameresult(
  client: GraphQlTestClient,
  slug: string,
  bggid = 31260,
  scoresOverride?: { playerName: string; score: number }[]
) {
  const scores = scoresOverride || [
    {
      playerName: 'Player 1',
      score: 100
    },
    {
      playerName: 'Player 2',
      score: 200
    },
    {
      playerName: 'Player 3',
      score: 300
    }
  ]

  const query = `
		mutation ($bggid: Int!, $slug: String!, $scores: [ScoreCreateInput!]!) {
			gameresult: createGameresult(data: {bggid: $bggid, slug: $slug, scores: $scores}) {
				id
				bggInfo {
					bggid
					name
				}
				scores {
					player {
						name
						id
					}
					score
				}
			}
		}
	`

  const variables = {
    slug,
    bggid,
    scores
  }

  const { gameresult } = await client.request(query, variables)

  return gameresult
}

export async function deleteGameResult(client: GraphQlTestClient, id: string) {
  const query = `
		mutation ($id: ID!) {
			deleteGameresult(where: {id: $id}) {
				id
			}
		}
	`
  const variables = {
    id
  }

  return await client.request(query, variables)
}

export async function updateGameResult(
  client: GraphQlTestClient,
  id: string,
  bggid = 31260,
  scores: { playerName: string; score: number }[]
) {
  const query = `
		mutation ($id: ID!, $bggid: Int!, $scores: [ScoreCreateInput!]!) {
			gameresult: updateGameresult(where: {id:$id}, data: {bggid: $bggid, scores: $scores}) {
				id
				bggInfo {
					bggid
					name
				}
				scores {
					player {
						name
						id
					}
					score
				}
			}
		}
	`

  const variables = {
    id,
    bggid,
    scores
  }

  return await client.request(query, variables)
}

export async function loadGameResult(client: GraphQlTestClient, id: string) {
  const query = `
		query ($id: ID!) {
			gameresult(where: {id: $id}) {
				id
				bggInfo {
				  bggid
				}
				scores {
				  player {
				    name
				  }
				  score
				}
			}
		}
	`
  const variables = {
    id
  }

  const { gameresult } = await client.request(query, variables)

  return gameresult
}

export async function loadTournamentMembers(
  client: GraphQlTestClient,
  slug: string
): Promise<TournamentMember[]> {
  const query = `
    query ($slug: String!) {
      tournament(where: { slug: $slug } ) {
        members {
          id
          role
          user {
            auth0id
          }
        }
      }
    }
  `

  const variables = {
    slug
  }

  const {
    tournament: { members }
  } = await client.request(query, variables)

  return members
}

export async function setMemberRole(
  client: GraphQlTestClient,
  tournamentMemberId: string,
  role: TournamentMemberRole
) {
  const query = `
		mutation ($id: ID!, $role: TournamentMemberRole!) {
			updateTournamentMember(where: {id: $id}, data: { role: $role }) {
				id
				role
      }
		}
	`
  const variables = {
    id: tournamentMemberId,
    role
  }

  const { updateTournamentMember } = await client.request(query, variables)

  return updateTournamentMember
}
