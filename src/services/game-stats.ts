import axios from 'axios'

interface GameResultScore {
  player: string
  score: number
}

interface GameResult {
  id: string
  time: string
  bggid: string
  scores: GameResultScore[]
}

export class GameStatsApi {
  private gameStatsUri: string

  constructor() {
    this.gameStatsUri = process.env.GAME_STATS_URI
  }

  async addGameresult(
    tournamentId: string,
    gameResult: GameResult
  ): Promise<GameResult> {
    const { data } = await axios.post<GameResult>(
      `${this.gameStatsUri}/tournament/${tournamentId}/gameresult`,
      gameResult
    )

    return data
  }

  async updateGameresult(tournamentId: string, gameResult: GameResult) {
    return await axios.put(
      `${this.gameStatsUri}/tournament/${tournamentId}/gameresult/${
        gameResult.id
      }`,
      gameResult
    )
  }

  async setGameWeight(bggid: string, weight: number): Promise<void> {
    await axios.post(`${this.gameStatsUri}/game/${bggid}/weight/${weight}`)
  }

  async deleteGameresult(tournamentId, gameResultId) {
    await axios.delete(
      `${
        this.gameStatsUri
      }/tournament/${tournamentId}/gameresult/${gameResultId}`
    )
  }
}

export const GameStats = new GameStatsApi()
