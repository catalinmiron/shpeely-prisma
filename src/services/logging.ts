import * as winston from 'winston'

winston.configure({
  level: process.env.LOG_LEVEL || 'debug',
  format: winston.format.cli(),
  transports: [new winston.transports.Console()]
})
