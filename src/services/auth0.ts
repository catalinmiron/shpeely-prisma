import axios from 'axios'

export interface UserProfile {
  avatar: string
  given_name?: string
  family_name?: string
  name?: string
  nickname: string
  picture: string
  gender: string
  updated_at: string // ISO-8601
  email: string
  email_verified: boolean
}

export class Auth0Api {
  private auth0Domain: string

  constructor() {
    this.auth0Domain = process.env.AUTH0_DOMAIN
  }

  async loadUserProfile(idToken: string): Promise<UserProfile> {
    const { data } = await axios.get<UserProfile>(
      `https://${this.auth0Domain}/userinfo`,
      {
        headers: {
          Authorization: `Bearer ${idToken}`
        }
      }
    )

    return data
  }
}

export const Auth0 = new Auth0Api()
