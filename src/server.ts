import { GraphQLServer } from 'graphql-yoga'
import { Prisma } from './generated/prisma'
import resolvers from './resolvers'
import { checkJwt } from './middleware/jwt'
import { getUser } from './middleware/getUser'
import './services/logging'
import * as logger from 'winston'
import { config } from 'dotenv'
import { IsTournamentMemberDirective } from './directives'

// Unfortunately there is no way to make the .env file optional except like this.
try {
  config()
  logger.debug('loaded .env file')
} catch {
  logger.debug('No .env file was loaded. Using system variables.')
}

logger.info(`Using Prisma endpoint ${process.env.PRISMA_ENDPOINT}`)

const db = new Prisma({
  endpoint: process.env.PRISMA_ENDPOINT, // the endpoint of the Prisma API (value set in `.env`)
  debug: true // log all GraphQL queries & mutations sent to the Prisma API
  // secret: process.env.PRISMA_SECRET, // only needed if specified in `database/prisma.yml` (value set in `.env`)
})

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  schemaDirectives: {
    isTournamentMember: IsTournamentMemberDirective
  },
  context: req => ({
    ...req,
    db
  })
})

server.express.post(
  server.options.endpoint,
  checkJwt,
  (err, req, res, next) => {
    // if (err) return res.status(401).send(err.message)
    next()
  }
)
server.express.post(server.options.endpoint, (req, res, next) =>
  getUser(req, res, next, db)
)

export default server
