# import TournamentWhereInput, TournamentOrderByInput from "./generated/prisma.graphql"
# import Gameresult from "./generated/prisma.graphql"

directive @isAuthenticated on QUERY | FIELD | MUTATION
directive @isTournamentMember(role: String) on QUERY | FIELD | MUTATION
directive @isOwner(type: String) on QUERY | MUTATION

type Query {
  me: User
  tournaments(
    where: TournamentWhereInput
    orderBy: TournamentOrderByInput
    skip: Int
    after: String
    before: String
    first: Int
    last: Int
  ): [Tournament!]!
  tournament(where: TournamentWhereUniqueInput!): Tournament
  gameresult(where: GameresultWhereUniqueInput!): Gameresult
  gameresults(
    where: GameresultWhereInput
    orderBy: GameresultOrderByInput
    skip: Int
    after: String
    before: String
    first: Int
    last: Int
  ): [Gameresult]!
  bggGameInfo(where: BggInfoWhereUniqueInput!): BggInfo!
  bggInfoes(
    where: BggInfoWhereInput
    orderBy: BggInfoOrderByInput
    skip: Int
    after: String
    before: String
    first: Int
    last: Int
  ): [BggInfo]!
  searchBggGame(search: String!): [BggSearchResult]!
  invitation(where: InvitationWhereUniqueInput!): Invitation
}

type Mutation {
  authenticate(idToken: String!): User!
  createTournament(data: TournamentCreateInput!): Tournament! @isAuthenticated
  createGameresult(data: GameResultCreateInput!): Gameresult! @isTournamentMember
  deleteGameresult(where: GameresultWhereUniqueInput!): Gameresult @isTournamentMember(role: "ADMIN")
  updateGameresult(data: GameResultUpdateInput!, where: GameresultWhereUniqueInput!): Gameresult @isTournamentMember(role: "ADMIN")
  createInvitation(data: InvitationCreateInput!): Invitation! @isTournamentMember
  acceptInvitation( where: InvitationWhereUniqueInput! ): Invitation! @isAuthenticated
  rejectInvitation( where: InvitationWhereUniqueInput! ): Invitation!
  updateTournamentMember( data: TournamentMemberUpdateInput!, where: TournamentMemberWhereUniqueInput! ): TournamentMember @isTournamentMember(role: "ADMIN")
}

type BggSearchResult {
  bggid: Int!
  year: Int!
  name: String!
}

type AuthPayload {
  token: String!
  user: User!
}

type User {
  auth0id: String!
  username: String!
  email: String @isTournamentMember(role: "ADMIN")
  avatar: String!
}

input TournamentCreateInput {
  name: String!
  gameresults: GameresultCreateManyWithoutTournamentInput
  players: PlayerCreateManyWithoutTournamentInput
  members: TournamentMemberCreateManyWithoutTournamentInput
}

input InvitationCreateInput {
  email: String!
  slug: String!
}

input GameResultCreateInput {
  bggid: Int!
  slug: String!
  time: DateTime
  scores: [ScoreCreateInput!]!
}

input GameResultUpdateInput {
  bggid: Int!
  scores: [ScoreCreateInput!]!
}

input ScoreCreateInput {
  playerName: String!
  score: Float!
}

type Tournament {
  slug: String!
  gameresults(
    where: GameresultWhereInput
    orderBy: GameresultOrderByInput
    skip: Int
    after: String
    before: String
    first: Int
    last: Int
  ): [Gameresult!]
  name: String!
  players(
    where: PlayerWhereInput
    orderBy: PlayerOrderByInput
    skip: Int
    after: String
    before: String
    first: Int
    last: Int
  ): [Player!]
  members(
    where: TournamentMemberWhereInput
    orderBy: TournamentMemberOrderByInput
    skip: Int
    after: String
    before: String
    first: Int
    last: Int
  ): [TournamentMember!]
  invitations(
    where: InvitationWhereInput
    orderBy: InvitationOrderByInput
    skip: Int
    after: String
    before: String
    first: Int
    last: Int
  ): [Invitation!]
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Invitation {
  token: String @isTournamentMember
  state: InvitationState!
  inviter(where: UserWhereInput): User!
  tournament(where: TournamentWhereInput): Tournament!
  createdAt: DateTime!
  updatedAt: DateTime!
}
