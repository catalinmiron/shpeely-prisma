#!/bin/sh

set -e

if [ -z "$PRISMA_ENDPOINT" ]; then
    echo "PRISMA_ENDPOINT is not defined. Exiting."
    exit 0
fi


if [ -z "$SKIP_DEPLOY" ]; then
    echo "Waiting for prisma to come online..."

    PRISMA_HOST_PORT=$(echo "http://prisma:4466" | sed 's/http:\/\///')
    ./wait-for-it.sh --timeout=60 $PRISMA_HOST_PORT

    echo "Starting prisma deployment..."
    echo "Prisma configuration:"
    cat prisma.yml

    prisma deploy

    echo "Prisma deployment complete."
else
    echo "Skipping prisma deploy..."
fi

exec "$@"
